(function($){
	
	//时间格式化
	$.extend({
		dateFtt: function (fmt, date) { //author: meizz   
			var o = {
				"M+": date.getMonth() + 1,                 //月份   
				"d+": date.getDate(),                    //日   
				"h+": date.getHours(),                   //小时   
				"m+": date.getMinutes(),                 //分   
				"s+": date.getSeconds(),                 //秒   
				"q+": Math.floor((date.getMonth() + 3) / 3), //季度   
				"S": date.getMilliseconds()             //毫秒   
			};
			if (/(y+)/.test(fmt))
				fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
			for (var k in o)
				if (new RegExp("(" + k + ")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			return fmt;
		}
	});

    //表单json格式化
	$.fn.serializeObject = function () {
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function () {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};

})(jQuery);

//字符串格式化扩展
String.prototype.format = function (args) {
    if (arguments.length > 0) {
        var result = this;
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                var reg = new RegExp("({" + key + "})", "g");
                result = result.replace(reg, args[key]);
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] == undefined) {
                    return "";
                }
                else {
                    var reg = new RegExp("({[" + i + "]})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
        return result;
    }
    else {
        return this;
    }
}


//获取当前时间
function getTime(needStr) {
	var addDayCount = function (dateObj, count, isString, noNeedZero) {
		var ddObj = dateObj || new Date();
		var dd = _.clone(ddObj);
		var hour = dd.getHours();
		var min = dd.getMinutes();
		var sec = dd.getSeconds();
		dd.setDate(dd.getDate() + count);
		var y = dd.getFullYear();
		var m = dd.getMonth();
		var d = dd.getDate();
		if (isString) {
			m += 1;
			if (!noNeedZero) {
				m = m < 10 ? "0" + m : m;
				d = d < 10 ? "0" + d : d;
				hour = hour < 10 ? "0" + hour : hour;
				min = min < 10 ? "0" + min : min;
				sec = sec < 10 ? "0" + sec : sec;
			}
			return stringformat("{0}-{1}-{2}T{3}:{4}:{5}", y, m, d, hour, min, sec);
		}
		return new Date(y, m, d, hour, min, sec);
	};
	var now = new Date();
	var nowYear = now.getFullYear();
	var nowMonth = now.getMonth() + 1;
	var nowDate = now.getDate();
	var nowHour = now.getHours();
	var nowMin = now.getMinutes();
	nowMonth = nowMonth < 10 ? "0" + nowMonth : nowMonth;
	nowDate = nowDate < 10 ? "0" + nowDate : nowDate;
	nowHour = nowHour < 10 ? "0" + nowHour : nowHour;
	nowMin = nowMin < 10 ? "0" + nowMin : nowMin;
	var startTime, endTime = stringformat("{0}-{1}-{2}T{3}:{4}:00", nowYear, nowMonth, nowDate, nowHour, nowMin);
	//早上8点到当前时间
	var desStr = "";
	if (nowHour >= 12) {
		startTime = stringformat("{0}-{1}-{2}T08:00:00", nowYear, nowMonth, nowDate);
		desStr = nowDate + "日8时至今";
	} else {
		var yesterday = addDayCount(now, -1);
		var yesYear = yesterday.getFullYear();
		var yesMonth = yesterday.getMonth() + 1;
		var yesDate = yesterday.getDate();
		yesMonth = yesMonth < 10 ? "0" + yesMonth : yesMonth;
		yesDate = yesDate < 10 ? "0" + yesDate : yesDate;
		startTime = stringformat("{0}-{1}-{2}T08:00:00", yesYear, yesMonth, yesDate);
		desStr = yesDate + "日8时至今";
	}
	if (needStr) {
		return {
			time: stringformat("[{0},{1}]", startTime, endTime),
			str: desStr
		};
	}
	return stringformat("[{0},{1}]", startTime, endTime);
	//return [ startTime, endTime ];
}