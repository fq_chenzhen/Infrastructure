﻿using Infrastructure.Lib.Core;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Xunit;

namespace DataProvider.Tests
{
    public class DemoTest
    {
         private List<UserInfo> _userList;
         private UserInfo _user;
         public DemoTest()
         {
            //fake data  
             _userList = new List<UserInfo>();
             _userList.Add(new UserInfo { UserID = 1, UserName="catcher", UserIsActive=true });
             _userList.Add(new UserInfo { UserID = 2, UserName = "hawk", UserIsActive = false });
             _user = new UserInfo() { UserID=3, UserIsActive=true, UserName = "tom" }; 
         }

         [Fact]
         public void get_all_users_should_success()
         {
             //arrange
             var fakeObject = new Mock<IUserDAL>();
             fakeObject.Setup(x=>x.GetAllUsers()).Returns(_userList);
             //act
             var res = fakeObject.Object.GetAllUsers();
             //assert
             Assert.Equal(2,res.Count);
         }

        [Fact]
        public void TestMethod1()
        {
            Assert.True(1 == 1);
        }

        [Theory]
        [InlineData(2, 3)]
        [InlineData(null, null)]
        public void TestTheory(int? i, int j)
        {
            Assert.False(i == j);
        }

        [Fact]
        public void Divide_Err()
        {
            Assert.ThrowsAny<Exception>(() =>
            {
                throw new Exception("除数不能为零");
            });
        }

        [Fact()]
        public void EncryptTest()
        {
            string key = "12345678";
            string actual = "TMR29YtnGPI=";
            DesHelper des = new DesHelper(Encoding.UTF8.GetBytes(key));
            Assert.Equal(des.Encrypt("admin"), actual);
            Assert.Equal(DesHelper.Encrypt("admin", key), actual);

            //弱密钥
            key = "123456781234567812345678";
            des = new DesHelper(Encoding.UTF8.GetBytes(key));
            Assert.Throws<CryptographicException>(() => des.Encrypt("admin"));

            key = "!@#$%^&*QWERTYUI12345678";
            actual = "Qp4r67VJ8Z0=";
            des = new DesHelper(Encoding.UTF8.GetBytes(key));
            Assert.Equal(des.Encrypt("admin"), actual);
            Assert.Equal(DesHelper.Encrypt("admin", key), actual);
        }

        [Fact]
        public void GetCpuId()
        {
            var CpuId = SystemInfoHandler.GetCpuId();
            Assert.Equal("BFEBFBFF000306C3", CpuId);
        }
    }

    public interface IUserDAL
    {
        IList<UserInfo> GetAllUsers();
    }

    public class UserInfo
    {
        public int UserID { get; set; }

        public string UserName { get; set; }

        public bool UserIsActive { get; set; }
    }
}
