﻿using Infrastructure.Lib.Core;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Linq;
using Dapper;
using Xunit;
using System.Data;
using Infrastructure.Lib.Core.Trilaterration.Model;
using System.Drawing;
using System.Collections.Generic;

namespace DataProvider.Tests
{
    public class InfrastructureLibCore
    {
        private readonly string connectionString = ConfigHelper.GetConnectionString("OracleConnStr");


        public OracleConnection CrateOracleConnection()
        {
            var connection = new OracleConnection(connectionString);
            connection.Open();
            return connection;
        }


        [Fact]
        public void TrilaterationTest()
        {
            Floor.Instance.AddBeancon("b1", new PointF(90, 114));
            Floor.Instance.AddBeancon("b2", new PointF(320, 196));
            Floor.Instance.AddBeancon("b3", new PointF(137, 416));

            var point = Floor.Instance.CalculateUserPosition(new List<LocationRadius>() {
                new LocationRadius{ Key="b1", Radius=127  },
                new LocationRadius{ Key="b2", Radius=149  },
                new LocationRadius{ Key="b3", Radius=205  },

            });

            Assert.True(point.X > 0); // 171, 213
        }

        [Fact]
        public void WsTest()
        {
            WebServiceAgent agent = new WebServiceAgent(@"http://61.154.79.86:9000/picservice/holdFujianServices.asmx");
            object[] args = new object[3];
            args[0] = "001ZQ007";
            args[1] = "2017-09-25 00:00:00";
            args[2] = "2017-09-25 23:59:59";
            string text = agent.Invoke("getOneStationImgInfos", args).ToString();

            Assert.NotNull(text);

        }

        [Fact]
        public void OrlQueryTest()
        {
            //dynamic p = new System.Dynamic.ExpandoObject();
            //p.a = "1";

            using (OracleConnection conn = CrateOracleConnection())
            {
                String executeSql = @"select * from SS_AREA where adcd = :adcd ";
                var conditon = new { adcd = "440402008002" };
                dynamic rlt = conn.Query(executeSql, conditon).SingleOrDefault();
                Assert.True(rlt.ADCD == "440402008002");
            }
        }

        [Fact]
        public void OrlInsertTest()
        {
            using (OracleConnection conn = CrateOracleConnection())
            {
                //IDbTransaction transaction = conn.BeginTransaction();
                String executeSql = @"insert into SS_AREA(adcd)values(:adcd)";

                var param = new { adcd = "1a" };//, Adnm = "test" };

                int row = conn.Execute(executeSql, param);//, transaction);
                //transaction.Commit();
                Assert.True(row > 0);
            }
        }


        [Fact]
        public void OrlDeleteTest()
        {
            using (OracleConnection conn = CrateOracleConnection())
            {
                IDbTransaction transaction = conn.BeginTransaction();
                String executeSql = @"delete from SS_AREA where adcd in :Adcds ";

                var param = new { Adcds = new string[] { "1a", "350922a", "44040200301a" } };

                int row = conn.Execute(executeSql, param, transaction, 3);
                transaction.Commit();
                Assert.True(row > 0);
            }
        }



        [Fact]
        public void OrlUpdateTest()
        {
            using (OracleConnection conn = CrateOracleConnection())
            {
                IDbTransaction transaction = conn.BeginTransaction();
                String executeSql = @"update SS_AREA set adnm = : adnm where adcd = :Adcd ";

                var param = new { ADCD = "440402008002", ADNM = "桂园社区" };

                int row = conn.Execute(executeSql, param, transaction, 3);
                transaction.Commit();
                Assert.True(row > 0);
            }
        }



    }
}
