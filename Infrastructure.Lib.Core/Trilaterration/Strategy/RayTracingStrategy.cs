﻿using System;
using System.Drawing;
using Infrastructure.Lib.Core.Trilaterration.Model;
using System.Collections.Generic;

namespace Infrastructure.Lib.Core.Trilaterration.Strategy
{
    public class RayTracingStrategy : AbstractStrategy
    {
        public override PointF CalculateUserPosition(List<BeconPrototype> beacons)
        {
            var wasIntersectionOfAllThree = false;
            var xMin = beacons[0].X;
            var xMax = xMin;
            var yMax = beacons[0].Y;
            var yMin = yMax;

            foreach (var beacon in beacons)
            {
                var x = beacon.X;
                var y = beacon.Y;
                var r = beacon.Accuracy;

                if (x - r < xMin)
                {
                    xMin = x - r;
                }
                if (x + r > xMax)
                {
                    xMax = x + r;
                }
                if (y - r < yMin)
                {
                    yMin = y - r;
                }
                if (y + r > yMax)
                {
                    yMax = y + r;
                }
            }

            var xMinUser = 1e10f;
            var xMaxUser = -1e10f;
            var yMinUser = 1e10f;
            var yMaxUser = -1e10f;
            for (var i = xMin; i < xMax; i += 0.07f)
            {
                for (var j = yMin; j < yMax; j += 0.07f)
                {
                    if (IsRayIntersectWithBeaconAccuracy(new PointF(i, j), beacons[0]) &&
                        IsRayIntersectWithBeaconAccuracy(new PointF(i, j), beacons[1]) &&
                        IsRayIntersectWithBeaconAccuracy(new PointF(i, j), beacons[2]))
                    {
                        wasIntersectionOfAllThree = true;
                        if (i < xMinUser)
                        {
                            xMinUser = i;
                        }
                        if (i > xMaxUser)
                        {
                            xMaxUser = i;
                        }

                        if (j < yMinUser)
                        {
                            yMinUser = j;
                        }
                        if (j > yMaxUser)
                        {
                            yMaxUser = j;
                        }
                    }
                }
            }
            return wasIntersectionOfAllThree ? 
                new PointF((xMinUser + xMaxUser) / 2.0f, (yMinUser + yMaxUser) / 2.0f) : 
                new PointF(-100, -100);
        }

        private static bool IsRayIntersectWithBeaconAccuracy(PointF ray, BeconPrototype beacon)
        {
            if (beacon == null)
                return false;

            var x = beacon.X;
            var y = beacon.Y;
            var r = beacon.Accuracy;
            return r > Math.Sqrt((ray.X - x) * (ray.X - x) + (ray.Y - y) * (ray.Y - y));
        }
    }
}
