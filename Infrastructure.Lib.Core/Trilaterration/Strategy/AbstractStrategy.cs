﻿using System.Drawing;
using Infrastructure.Lib.Core.Trilaterration.Model;
using System.Collections.Generic;

namespace Infrastructure.Lib.Core.Trilaterration.Strategy
{
    public abstract class AbstractStrategy
    {
        public abstract PointF CalculateUserPosition(List<BeconPrototype> beacons);
    }
}
