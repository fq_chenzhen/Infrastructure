﻿using System;
using System.Drawing;

namespace Infrastructure.Lib.Core.Trilaterration.Model
{
    public class BeconPrototype
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Accuracy { get; set; }

        public BeconPrototype()
        {
            Accuracy = -1;
        }

        public BeconPrototype(PointF positon)
        {
            X = positon.X;
            Y = positon.Y;
            Accuracy = -1;
        }



        public BeconPrototype(float x, float y)
        {
            X = x;
            Y = y;
            Accuracy = -1;
        }

        public void RefreshAccuracy(float x, float y, float ratio)
        {
            var xM = x / ratio - X;
            var yM = y / ratio - Y;
            Accuracy = (float)Math.Sqrt(xM * xM + yM * yM);
        }

        public PointF PositionAsPoint()
        {
            return new PointF(X, Y);
        }

        public BeconPrototype Clone()
        {
            return (BeconPrototype)this.MemberwiseClone();
        }

    }
}
