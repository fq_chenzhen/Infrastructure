﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Lib.Core.Trilaterration.Model
{
    public class BeconManager
    {
        class BeconNested
        {
            static BeconNested() { }

            internal readonly static BeconManager instance = new BeconManager();
        }

        public static BeconManager Instance
        {
            get
            {
                return BeconNested.instance;
            }
        }

        private Dictionary<string, BeconPrototype> Becons = new Dictionary<string, BeconPrototype>();

        private BeconManager() { }

        public BeconPrototype this[string key]
        {
            get
            {
                if (Becons.ContainsKey(key))
                    return Becons[key];
                else
                    return null;
            }

            set
            {
                if (!Becons.ContainsKey(key))
                    Becons.Add(key, value);
            }

        }
    }
}
