﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Lib.Core.Trilaterration.Model
{
    public class LocationRadius
    {
        public string Key { get; set; }

        public float Radius { get; set; }

    }
}
