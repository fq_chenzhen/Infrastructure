﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Infrastructure.Lib.Core
{
    public static partial class FileHelper
    {
        /*头文件参考：（我自己测是如有偏差请联系我）
            7790：exe，dll

            5666：psd
            6677：bmp
            7173：gif
            13780：png
            255216：jpg,jpeg
        
            239187：js
            6787：swf
            7368：mp3
            4838：wma

            6063：xml

            8297：rar
            55122：7z
            8075：docx，xlsx，pptx，vsdx，mmap，xmind，“zip”
            208207：doc，xls，ppt，mpp，vsd
       */
        /// <summary>
        /// 判断扩展名是否是指定类型---默认是判断图片格式，符合返回true（没有释放stream，请手动：file.InputStream.Dispose();）
        /// eg：图片+压缩+文档："7173", "255216", "6677", "13780", "8297", "55122", "8075", "208207"
        /// eg：img，"7173", "255216", "6677", "13780" //gif  //jpg  //bmp //png
        /// eg：file,"8297", "55122", "8075", "208207" //rar //7z //zip + 文档系列
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="fileTypes">文件扩展名</param>
        /// <returns></returns>
        public static bool CheckingExt(this Stream stream, params string[] fileTypes)
        {
            if (fileTypes == null || fileTypes.Length == 0) { fileTypes = new string[] { "7173", "255216", "6677", "13780" }; }
            bool result = false;
            string fileclass = "";

            #region 读取头两个字节
            var reader = new BinaryReader(stream);
            byte[] buff = new byte[2];
            try
            {
                reader.Read(buff, 0, 2);//读取每个文件的头两个字节
                fileclass = buff[0].ToString() + buff[1].ToString();
            }
            catch//(System.Exception ex)
            {
                stream.Dispose();
                reader.Dispose();
                return false;
            }
            #endregion

            #region 校验
            for (int i = 0; i < fileTypes.Length; i++)
            {
                if (fileclass == fileTypes[i])
                {
                    result = true;
                    break;
                }
            }
            #endregion
            return result;
        }

        /// <summary>
        /// 删除文件（到回收站[可选]）
        /// </summary>
        /// <param name="filename">要删除的文件名</param>
        /// <param name="isSendToRecycleBin">是否删除到回收站</param>
        public static void Delete(string filename, bool isSendToRecycleBin = false)
        {
            if (isSendToRecycleBin)
            {
                FileSystem.DeleteFile(filename, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
            }
            else
            {
                File.Delete(filename);
            }
        }

        /// <summary>
        /// 设置或取消文件的指定<see cref="FileAttributes"/>属性
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <param name="attribute">要设置的文件属性</param>
        /// <param name="isSet">true为设置，false为取消</param>
        public static void SetAttribute(string fileName, FileAttributes attribute, bool isSet)
        {
            FileInfo fi = new FileInfo(fileName);
            if (!fi.Exists)
            {
                throw new FileNotFoundException("要设置属性的文件不存在。", fileName);
            }
            if (isSet)
            {
                fi.Attributes = fi.Attributes | attribute;
            }
            else
            {
                fi.Attributes = fi.Attributes & ~attribute;
            }
        }

        /// <summary>
        /// 获取文件版本号
        /// </summary>
        /// <param name="fileName"> 完整文件名 </param>
        /// <returns> 文件版本号 </returns>
        public static string GetVersion(string fileName)
        {
            if (File.Exists(fileName))
            {
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(fileName);
                return fvi.FileVersion;
            }
            return null;
        }

        /// <summary>
        /// 获取文件的MD5值
        /// </summary>
        /// <param name="fileName"> 文件名 </param>
        /// <returns> 32位MD5 </returns>
        public static string GetFileMd5(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            const int bufferSize = 1024 * 1024;
            byte[] buffer = new byte[bufferSize];

            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.Initialize();

            long offset = 0;
            while (offset < fs.Length)
            {
                long readSize = bufferSize;
                if (offset + readSize > fs.Length)
                {
                    readSize = fs.Length - offset;
                }
                fs.Read(buffer, 0, (int)readSize);
                if (offset + readSize < fs.Length)
                {
                    md5.TransformBlock(buffer, 0, (int)readSize, buffer, 0);
                }
                else
                {
                    md5.TransformFinalBlock(buffer, 0, (int)readSize);
                }
                offset += bufferSize;
            }
            fs.Close();
            byte[] result = md5.Hash;
            md5.Clear();
            StringBuilder sb = new StringBuilder(32);
            foreach (byte b in result)
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

        #region 检测指定目录是否存在
        /// <summary>  
        /// 检测指定目录是否存在  
        /// </summary>  
        /// <param name="directoryPath">目录的绝对路径</param>          
        public static bool IsExistDirectory(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }
        #endregion

        #region 检测指定文件是否存在
        /// <summary>  
        /// 检测指定文件是否存在,如果存在则返回true。  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>          
        public static bool IsExistFile(string filePath)
        {
            return File.Exists(filePath);
        }
        #endregion

        #region 检测指定目录是否为空
        /// <summary>  
        /// 检测指定目录是否为空  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>          
        public static bool IsEmptyDirectory(string directoryPath)
        {
            try
            {
                //判断是否存在文件  
                string[] fileNames = GetFileNames(directoryPath);
                if (fileNames.Length > 0)
                {
                    return false;
                }

                //判断是否存在文件夹  
                string[] directoryNames = GetDirectories(directoryPath);
                return directoryNames.Length <= 0;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region 检测指定目录中是否存在指定的文件
        /// <summary>  
        /// 检测指定目录中是否存在指定的文件,若要搜索子目录请使用重载方法.  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>  
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。  
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param>          
        public static bool Contains(string directoryPath, string searchPattern)
        {
            try
            {
                //获取指定的文件列表  
                string[] fileNames = GetFileNames(directoryPath, searchPattern, false);

                //判断指定文件是否存在  
                return fileNames.Length != 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>  
        /// 检测指定目录中是否存在指定的文件  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>  
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。  
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param>   
        /// <param name="isSearchChild">是否搜索子目录</param>  
        public static bool Contains(string directoryPath, string searchPattern, bool isSearchChild)
        {
            try
            {
                //获取指定的文件列表  
                string[] fileNames = GetFileNames(directoryPath, searchPattern, true);

                //判断指定文件是否存在  
                return fileNames.Length != 0;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region 创建一个目录
        /// <summary>  
        /// 创建一个目录  
        /// </summary>  
        /// <param name="directoryPath">目录的绝对路径</param>  
        public static void CreateDirectory(string directoryPath)
        {
            //如果目录不存在则创建该目录  
            if (!IsExistDirectory(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }
        #endregion

        #region 创建一个文件
        /// <summary>  
        /// 创建一个文件。  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        public static bool CreateFile(string filePath)
        {
            try
            {
                //如果文件不存在则创建该文件  
                if (!IsExistFile(filePath))
                {
                    //创建一个FileInfo对象  
                    FileInfo file = new FileInfo(filePath);
                    //创建文件  
                    FileStream fs = file.Create();
                    //关闭文件流  
                    fs.Close();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>  
        /// 创建一个文件,并将字节流写入文件。  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        /// <param name="buffer">二进制流数据</param>  
        public static bool CreateFile(string filePath, byte[] buffer)
        {
            try
            {
                //如果文件不存在则创建该文件  
                if (!IsExistFile(filePath))
                {
                    //创建一个FileInfo对象  
                    FileInfo file = new FileInfo(filePath);

                    //创建文件  
                    FileStream fs = file.Create();

                    //写入二进制流  
                    fs.Write(buffer, 0, buffer.Length);

                    //关闭文件流  
                    fs.Close();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        #endregion

        #region 获取文本文件的行数
        /// <summary>  
        /// 获取文本文件的行数  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>          
        public static int GetLineCount(string filePath)
        {
            //将文本文件的各行读到一个字符串数组中  
            string[] rows = File.ReadAllLines(filePath);

            //返回行数  
            return rows.Length;
        }
        #endregion

        #region 获取一个文件的长度
        /// <summary>  
        /// 获取一个文件的长度,单位为Byte  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>          
        public static int GetFileSize(string filePath)
        {
            //创建一个文件对象  
            FileInfo fi = new FileInfo(filePath);

            //获取文件的大小  
            return (int)fi.Length;
        }

        /// <summary>  
        /// 获取一个文件的长度,单位为KB  
        /// </summary>  
        /// <param name="filePath">文件的路径</param>          
        public static double GetFileSizeByKB(string filePath)
        {
            //创建一个文件对象  
            FileInfo fi = new FileInfo(filePath);
            long size = fi.Length / 1024;
            //获取文件的大小  
            return double.Parse(size.ToString());
        }

        /// <summary>  
        /// 获取一个文件的长度,单位为MB  
        /// </summary>  
        /// <param name="filePath">文件的路径</param>          
        public static double GetFileSizeByMB(string filePath)
        {
            //创建一个文件对象  
            FileInfo fi = new FileInfo(filePath);
            long size = fi.Length / 1024 / 1024;
            //获取文件的大小  
            return double.Parse(size.ToString());
        }
        #endregion

        #region 获取指定目录中的文件列表
        /// <summary>  
        /// 获取指定目录中所有文件列表  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>          
        public static string[] GetFileNames(string directoryPath)
        {
            //如果目录不存在，则抛出异常  
            if (!IsExistDirectory(directoryPath))
            {
                throw new FileNotFoundException();
            }

            //获取文件列表  
            return Directory.GetFiles(directoryPath);
        }

        /// <summary>  
        /// 获取指定目录及子目录中所有文件列表  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>  
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。  
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param>  
        /// <param name="isSearchChild">是否搜索子目录</param>  
        public static string[] GetFileNames(string directoryPath, string searchPattern, bool isSearchChild)
        {
            //如果目录不存在，则抛出异常  
            if (!IsExistDirectory(directoryPath))
            {
                throw new FileNotFoundException();
            }

            try
            {
                return Directory.GetFiles(directoryPath, searchPattern, isSearchChild ? System.IO.SearchOption.AllDirectories : System.IO.SearchOption.TopDirectoryOnly);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region 获取指定目录中的子目录列表
        /// <summary>  
        /// 获取指定目录中所有子目录列表,若要搜索嵌套的子目录列表,请使用重载方法.  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>          
        public static string[] GetDirectories(string directoryPath)
        {
            try
            {
                return Directory.GetDirectories(directoryPath);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>  
        /// 获取指定目录及子目录中所有子目录列表  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>  
        /// <param name="searchPattern">模式字符串，"*"代表0或N个字符，"?"代表1个字符。  
        /// 范例："Log*.xml"表示搜索所有以Log开头的Xml文件。</param>  
        /// <param name="isSearchChild">是否搜索子目录</param>  
        public static string[] GetDirectories(string directoryPath, string searchPattern, bool isSearchChild)
        {
            try
            {
                return Directory.GetDirectories(directoryPath, searchPattern, isSearchChild ? System.IO.SearchOption.AllDirectories : System.IO.SearchOption.TopDirectoryOnly);
            }
            catch
            {
                throw null;
            }
        }
        #endregion

        #region 向文本文件写入内容
        /// <summary>  
        /// 向文本文件中写入内容  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        /// <param name="content">写入的内容</param>          
        public static void WriteText(string filePath, string content)
        {
            //向文件写入内容  
            File.WriteAllText(filePath, content);
        }
        #endregion

        #region 向文本文件的尾部追加内容
        /// <summary>  
        /// 向文本文件的尾部追加内容  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        /// <param name="content">写入的内容</param>  
        public static void AppendText(string filePath, string content)
        {
            File.AppendAllText(filePath, content);
        }
        #endregion

        #region 将现有文件的内容复制到新文件中
        /// <summary>  
        /// 将源文件的内容复制到目标文件中  
        /// </summary>  
        /// <param name="sourceFilePath">源文件的绝对路径</param>  
        /// <param name="destFilePath">目标文件的绝对路径</param>  
        public static void Copy(string sourceFilePath, string destFilePath)
        {
            File.Copy(sourceFilePath, destFilePath, true);
        }
        #endregion

        #region 将文件移动到指定目录
        /// <summary>  
        /// 将文件移动到指定目录  
        /// </summary>  
        /// <param name="sourceFilePath">需要移动的源文件的绝对路径</param>  
        /// <param name="descDirectoryPath">移动到的目录的绝对路径</param>  
        public static void Move(string sourceFilePath, string descDirectoryPath)
        {
            //获取源文件的名称  
            string sourceFileName = GetFileName(sourceFilePath);

            if (IsExistDirectory(descDirectoryPath))
            {
                //如果目标中存在同名文件,则删除  
                if (IsExistFile(descDirectoryPath + "\\" + sourceFileName))
                {
                    DeleteFile(descDirectoryPath + "\\" + sourceFileName);
                }
                //将文件移动到指定目录  
                File.Move(sourceFilePath, descDirectoryPath + "\\" + sourceFileName);
            }
        }
        #endregion

        #region 将流读取到缓冲区中
        /// <summary>  
        /// 将流读取到缓冲区中  
        /// </summary>  
        /// <param name="stream">原始流</param>  
        public static byte[] StreamToBytes(Stream stream)
        {
            try
            {
                //创建缓冲区  
                byte[] buffer = new byte[stream.Length];

                //读取流  
                stream.Read(buffer, 0, int.Parse(stream.Length.ToString()));

                //返回流  
                return buffer;
            }
            catch
            {
                return null;
            }
            finally
            {
                //关闭流  
                stream.Close();
            }
        }
        #endregion

        #region 将文件读取到缓冲区中
        /// <summary>  
        /// 将文件读取到缓冲区中  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        public static byte[] FileToBytes(string filePath)
        {
            //获取文件的大小   
            int fileSize = GetFileSize(filePath);

            //创建一个临时缓冲区  
            byte[] buffer = new byte[fileSize];

            //创建一个文件流  
            FileInfo fi = new FileInfo(filePath);
            FileStream fs = fi.Open(FileMode.Open);

            try
            {
                //将文件流读入缓冲区  
                fs.Read(buffer, 0, fileSize);

                return buffer;
            }
            catch
            {
                return null;
            }
            finally
            {
                //关闭文件流  
                fs.Close();
            }
        }
        #endregion

        #region 将文件读取到字符串中
        /// <summary>  
        /// 将文件读取到字符串中  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        public static string FileToString(string filePath)
        {
            return FileToString(filePath, Encoding.Default);
        }
        /// <summary>  
        /// 将文件读取到字符串中  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        /// <param name="encoding">字符编码</param>  
        public static string FileToString(string filePath, Encoding encoding)
        {
            //创建流读取器  
            StreamReader reader = new StreamReader(filePath, encoding);
            try
            {
                //读取流  
                return reader.ReadToEnd();
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                //关闭流读取器  
                reader.Close();
            }
        }
        #endregion

        #region 从文件的绝对路径中获取文件名( 包含扩展名 )
        /// <summary>  
        /// 从文件的绝对路径中获取文件名( 包含扩展名 )  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>          
        public static string GetFileName(string filePath)
        {
            //获取文件的名称  
            FileInfo fi = new FileInfo(filePath);
            return fi.Name;
        }
        #endregion

        #region 从文件的绝对路径中获取文件名( 不包含扩展名 )
        /// <summary>  
        /// 从文件的绝对路径中获取文件名( 不包含扩展名 )  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>          
        public static string GetFileNameNoExtension(string filePath)
        {
            //获取文件的名称  
            FileInfo fi = new FileInfo(filePath);
            return fi.Name.Split('.')[0];
        }
        #endregion

        #region 从文件的绝对路径中获取扩展名
        /// <summary>  
        /// 从文件的绝对路径中获取扩展名  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>          
        public static string GetExtension(string filePath)
        {
            //获取文件的名称  
            FileInfo fi = new FileInfo(filePath);
            return fi.Extension;
        }
        #endregion

        #region 清空指定目录
        /// <summary>  
        /// 清空指定目录下所有文件及子目录,但该目录依然保存.  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>  
        public static void ClearDirectory(string directoryPath)
        {
            if (IsExistDirectory(directoryPath))
            {
                //删除目录中所有的文件  
                string[] fileNames = GetFileNames(directoryPath);
                foreach (string t in fileNames)
                {
                    DeleteFile(t);
                }

                //删除目录中所有的子目录  
                string[] directoryNames = GetDirectories(directoryPath);
                foreach (string t in directoryNames)
                {
                    DeleteDirectory(t);
                }
            }
        }
        #endregion

        #region 清空文件内容
        /// <summary>  
        /// 清空文件内容  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        public static void ClearFile(string filePath)
        {
            //删除文件  
            File.Delete(filePath);

            //重新创建该文件  
            CreateFile(filePath);
        }
        #endregion

        #region 删除指定文件
        /// <summary>  
        /// 删除指定文件  
        /// </summary>  
        /// <param name="filePath">文件的绝对路径</param>  
        public static void DeleteFile(string filePath)
        {
            if (IsExistFile(filePath))
            {
                File.Delete(filePath);
            }
        }
        #endregion

        #region 删除指定目录
        /// <summary>  
        /// 删除指定目录及其所有子目录  
        /// </summary>  
        /// <param name="directoryPath">指定目录的绝对路径</param>  
        public static void DeleteDirectory(string directoryPath)
        {
            if (IsExistDirectory(directoryPath))
            {
                Directory.Delete(directoryPath, true);
            }
        }
        #endregion

        //判断文件是否被占用
        public static bool FileIsUsed(string fileFullName)
        {
            bool result = false;
            //判断文件是否存在，如果不存在，直接返回 false
            if (!System.IO.File.Exists(fileFullName))
            {
                result = false;
            }//end: 如果文件不存在的处理逻辑
            else
            {//如果文件存在，则继续判断文件是否已被其它程序使用
                //逻辑：尝试执行打开文件的操作，如果文件已经被其它程序使用，则打开失败，抛出异常，根据此类异常可以判断文件是否已被其它程序使用。
                System.IO.FileStream fileStream = null;
                try
                {
                    fileStream = System.IO.File.Open(fileFullName, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
                    result = false;
                }
                catch (System.IO.IOException ioEx)
                {
                    result = true;
                }
                catch (System.Exception ex)
                {
                    result = true;
                }
                finally
                {
                    if (fileStream != null)
                    {
                        fileStream.Close();
                    }
                }
            }//end: 如果文件存在的处理逻辑
            //返回指示文件是否已被其它程序使用的值
            return result;
        }

        //判断程序是否准备好
        public static bool IsFileReady(string fullPath)
        {
            int waitTime = 0;
            int timeout = 60 * 1000; // 等待超时时间，1分钟
            int checkTime = 100; // 轮询等待时间，0.1秒
            do
            {
                if (waitTime > timeout)
                {
                    return false; // 等待超时
                }

                try
                {
                    using (FileStream fs = File.Open(fullPath, FileMode.Open, FileAccess.Read, FileShare.None))
                    { }
                    return true; // 文件准备就绪
                }
                catch
                {
                    System.Threading.Thread.Sleep(checkTime); // 等待0.1秒
                    waitTime += checkTime;
                }
            } while (true);
        }

        public static string CurrentDir()
        {
            return new FileInfo(Uri.UnescapeDataString(new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath)).DirectoryName;
        }
    }
}