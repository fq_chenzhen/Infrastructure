﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;

namespace Infrastructure.Lib.Core.Cache
{
    public class CacheLayer
    {
        static readonly ObjectCache Cache = MemoryCache.Default;
        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)Cache[key];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add<T>(T objectToCache, string key) where T : class
        {
            Cache.Add(key, objectToCache, DateTime.Now.AddDays(7));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Set<T>(T objectToCache, string key) where T : class
        {
            Cache.Set(key, objectToCache, DateTime.Now.AddDays(7));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="cacheSecond">缓存的秒数</param>
        public static void Add<T>(T objectToCache, string key, int cacheSecond = 7200) where T : class
        {
            Cache.Add(key, objectToCache, new DateTimeOffset(DateTime.Now.AddSeconds(cacheSecond)));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        public static void Add(object objectToCache, string key)
        {
            Cache.Add(key, objectToCache, DateTime.Now.AddDays(7));
        }

        /// <summary>
        /// Insert value into the cache using
        /// appropriate name/value pairs
        /// </summary>
        /// <param name="objectToCache">Item to be cached</param>
        /// <param name="key">Name of item</param>
        /// <param name="cacheMinute">缓存多少分钟</param>
        public static void Add(object objectToCache, string key, int cacheMinute = 30)
        {
            Cache.Add(key, objectToCache, DateTime.Now.AddMinutes(cacheMinute));
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            if (Cache.Contains(key)) return true;
            return false;
        }

        /// <summary>
        /// Gets all cached items as a list by their key.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAll()
        {
            return Cache.Select(keyValuePair => keyValuePair.Key).ToList();
        }

        public static object Remove(string key)
        {
            if (CacheLayer.Exists(key))
                return Cache.Remove(key);

            return null;
        }
    }
}
