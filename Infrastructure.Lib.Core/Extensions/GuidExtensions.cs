﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/************************************************************************************************************
* author 	:  YLH-PC
* function	:  
* history	:  created by YLH-PC 6/22/2015 11:54:58 AM 
************************************************************************************************************/
namespace Infrastructure.Lib.Core
{
    public static class GuidExtensions
    {
        /// <summary>
        /// 将GUID转换为字符串类型，并去除中横线
        /// </summary>
        /// <param name="guid">Guid</param>
        /// <returns></returns>
        public static string ToStringAndRemoveHyphen(this Guid guid)
        {
            return guid.ToString().Replace("-", "");
        }
    }
}
