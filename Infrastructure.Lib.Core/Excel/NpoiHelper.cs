﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using HorizontalAlignment = System.Windows.Forms.HorizontalAlignment;

namespace Infrastructure.Lib.Core.Excel
{
    public class NPOIHelper
    {
        //
        //        //1.获取模块的完整路径。 
        //        string path1 = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
        //
        ////2.获取和设置当前目录(该进程从中启动的目录)的完全限定目录 
        //        string path2 = System.Environment.CurrentDirectory;
        //
        ////3.获取应用程序的当前工作目录 
        //        string path3 = System.IO.Directory.GetCurrentDirectory();
        //
        ////4.获取程序的基目录 
        //        string path4 = System.AppDomain.CurrentDomain.BaseDirectory;
        //
        ////5.获取和设置包括该应用程序的目录的名称 
        //        string path5 = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
        //
        ////6.获取启动了应用程序的可执行文件的路径 
        //        string path6 = System.Windows.Forms.Application.StartupPath;
        //
        ////7.获取启动了应用程序的可执行文件的路径及文件名 
        //        string path7 = System.Windows.Forms.Application.ExecutablePath; 

        #region 导出方法

        /// <summary>
        ///  JToken exportData = JToken.Parse("[" + string.Join(",", tempList) + "]");
        /// </summary>
        /// <param name="titleConfig"></param>
        /// <param name="fieldConfig"></param>
        /// <param name="data"></param>
        /// <param name="titel"></param>
        /// <returns></returns>
        public static string ExportCustomerToExcel(JToken titleConfig, JToken fieldConfig, JToken data, string titel)
        {
            var book = new HSSFWorkbook();
            ICellStyle cellStyle = book.CreateCellStyle();
            cellStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            IFont font = book.CreateFont();
            font.FontHeightInPoints = 12;
            font.Boldweight = (short)FontBoldWeight.Bold;
            cellStyle.SetFont(font);

            ISheet sheet = book.CreateSheet("Sheet1");
            IRow drow = sheet.CreateRow(0);
            titleConfig.ForIndexEach((i, e1) =>
            {
                ICell cell = drow.CreateCell(i, CellType.String);
                cell.SetCellValue(e1.ToString());
                cell.CellStyle = cellStyle;
            });
            data.ForIndexEach((i, e1) =>
            {
                drow = sheet.CreateRow(i + 1);
                fieldConfig.ForIndexEach((j, e2) =>
                {
                    ICell cell = drow.CreateCell(j, CellType.String);
                    cell.SetCellValue(e1[e2.ToString()].Value<string>());
                });
            });
            fieldConfig.ForIndexEach((j, e2) =>
            {
                sheet.AutoSizeColumn(j, true);
            });

            //var saveRoot = TypeHelper.CreateInstance<FileSaveDirectoryConfig>().FileExportDirectory;

            //var saveRoot = $"{_hostingEnvironment.ContentRootPath}{@"\"}Exprot{@"\"}{string.Format("{0:yyyyMMdd}", DateTime.Now)}{@"\"}";

            var saveRoot = System.AppDomain.CurrentDomain.BaseDirectory + @"\Exprot\" + string.Format("{0:yyyyMMdd}", DateTime.Now) + @"\";

            if (!Directory.Exists(saveRoot))
            {
                Directory.CreateDirectory(saveRoot);
            }
            string fileName = titel + string.Format("{0:yyyyMMddHHmmssffff}", DateTime.Now) + ".xls";
            fileName = Path.Combine(saveRoot, fileName);
            using (var fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write))
            {
                book.Write(fs);
            }
            return fileName;
        }
        #endregion 导出方法
    }
}
