﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Lib.Core
{
    public static class BitmapHeler
    {
        /*
   *bitmap转base64
   */
        public static String BitmapToBase64(Bitmap bitmap)
        {
            String result = "";
            try
            {
                using (MemoryStream ms1 = new MemoryStream())
                {
                    bitmap.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] arr1 = new byte[ms1.Length];
                    ms1.Position = 0;
                    ms1.Read(arr1, 0, (int)ms1.Length);
                    ms1.Close();
                    result = Convert.ToBase64String(arr1);
                }
            }
            catch (Exception e)
            {
               
            }
            return result;
        }


        public static Bitmap Base64ToBitmap(string photo)
        {
            System.Drawing.Bitmap bmp2;
              //将Base64String转为图片并保存
            byte[] arr2 = Convert.FromBase64String(photo);
            using (MemoryStream ms2 = new MemoryStream(arr2))
            {
                bmp2 = new System.Drawing.Bitmap(ms2);
                //bmp2.Save(filePath + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                //bmp2.Save(filePath + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                //bmp2.Save(filePath + ".gif", System.Drawing.Imaging.ImageFormat.Gif);
                //bmp2.Save(filePath + ".png", System.Drawing.Imaging.ImageFormat.Png);
            }

            return bmp2;
        }

    }
}
