﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Lib.Core
{
    public class Base64Helper
    {
        public static void Base64SaveToJpeg(string photo, string filePath)
        {
            System.Drawing.Bitmap bmp2;
            //将Base64String转为图片并保存
            byte[] arr2 = Convert.FromBase64String(photo);
            using (MemoryStream ms2 = new MemoryStream(arr2))
            {
                bmp2 = new System.Drawing.Bitmap(ms2);
                bmp2.Save(filePath + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                //bmp2.Save(filePath + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                //bmp2.Save(filePath + ".gif", System.Drawing.Imaging.ImageFormat.Gif);
                //bmp2.Save(filePath + ".png", System.Drawing.Imaging.ImageFormat.Png);
            }
        }

    }

}
