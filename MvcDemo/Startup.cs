﻿using Autofac;
using Autofac.Configuration;
using Autofac.Integration.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Owin;
using MvcDemo.Model;
using Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(MvcDemo.Startup))]
namespace MvcDemo
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            // STANDARD MVC SETUP:

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterModelBinders(Assembly.Load("Infrastructure.Lib.RESTful"));
            builder.RegisterModelBinderProvider();

            // Run other optional steps, like registering model binders,
            // web abstractions, etc., then set the dependency resolver
            // to be Autofac.
            //var configPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\Config");
            //builder.RegisterModule(new ConfigurationSettingsReader("components", Path.Combine(configPath, "components.xml")));
            //builder.RegisterType<PictureDiagnosisRepository>().As<IPictureDiagnosisRepository>().InstancePerDependency();
            //builder.RegisterType<PictureStConfigRepository>().As<IPictureStConfigRepository>().InstancePerDependency();
            //builder.RegisterType<PictureAuthorizeRepository>().As<IPictureAuthorizeRepository>().InstancePerDependency();
            //builder.RegisterType<PicturePlatformRepository>().As<IPicturePlatformRepository>().InstancePerDependency();

            //builder.RegisterType<PictureDiagnosisService>().As<IPictureDiagnosisService>().InstancePerDependency();
            //builder.RegisterType<PictureStConfigService>().As<IPictureStConfigService>().InstancePerDependency();
            //builder.RegisterType<PictureAuthorizeService>().As<IPictureAuthorizeService>().InstancePerDependency();
            //builder.RegisterType<PicturePlatformService>().As<IPicturePlatformService>().InstancePerDependency();

            builder.RegisterType<Output>().As<IOutput>().InstancePerDependency();

            builder.RegisterModule<AutofacWebTypesModule>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));


            // OWIN MVC SETUP:

            // Register the Autofac middleware FIRST, then the Autofac MVC middleware.
            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

        }
    }
}
