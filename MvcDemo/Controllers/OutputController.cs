﻿using MvcDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcDemo.Controllers
{
    public class OutputController : Controller
    {
        private IOutput writer;

        public OutputController(IOutput writer)
        {
            this.writer = writer;
        }

        [HttpGet]
        public string Write(string msg)
        {
            return this.writer.Write(msg);
        }
	}
}