﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(SignalROwinHost.Startup))]

namespace SignalROwinHost
{
    public class Startup
    {
        // This method name is important
        public void Configuration(IAppBuilder app)
        {
            //跨域设置  
            app.UseCors(CorsOptions.AllowAll);

            //路由注册(使用默认)  
            app.MapSignalR();  

            app.UseStaticFiles();
        }
    }
}
