﻿using Microsoft.AspNet.SignalR.Client;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SignalROwinHost
{
    class Program
    {
        private static IHubProxy _msgProxy;
        static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
            Task.Run(() => SignalRTask(cts.Token), cts.Token);

            while (true)
            {
                var x = Console.ReadLine();
                if (x == "quit")
                {
                    break;
                }
                else
                {
                    proxSignalRMessage(x);
                }
                Thread.Sleep(1000);
            }
        }

        private static void SignalRTask(CancellationToken token)
        {
            //创建服务器监听  
            var url = "http://*:8000";
            using (WebApp.Start(url))
            {
                Console.WriteLine("SignalR Server runing on {0}", url);

                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        _msgProxy = null;
                        break;
                    }
                    Thread.Sleep(1000);
                }
            }
        }

        private static void proxSignalRMessage(string msg)
        {
            if (_msgProxy == null)
            {
                //创建客户端连接  
                string url = "http://127.0.0.1:8000";
                var con = new HubConnection(url, "name=ConsoleClient");

                //var con = new HubConnection(url);
                //实例化客户端代理  
                _msgProxy = con.CreateHubProxy("MsgHub");
                //注册客户端接收  
                _msgProxy.On("receive", (content) =>
                {
                    Console.WriteLine("服务器推送内容：" + content);
                });
                //启动监听并等待  
                con.Start().Wait();
                Console.WriteLine("ConsoleClient,启动成功");
            }

            //new int[] { 1, 2, 3 }.ObjectToJson()

            //客户端，发送内容  
            _msgProxy.Invoke("SendPrivateMessage", "WebClient", msg);
        }
    }
}
