﻿using AnimatedGif;
using ImageProcessor;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Picture
{
    class Program
    {
        static void Main(string[] args)
        {
            var gifAbsPath = "";
            List<string> picList = new List<string>();

            using (AnimatedGifCreator gifCreator = AnimatedGif.AnimatedGif.Create(gifAbsPath, 200))
            {
                //Enumerate through a List<System.Drawing.Image> or List<System.Drawing.Bitmap> for example
                foreach (var imgPath in picList)
                {
                    //if (imgPath.IsNullOrEmpty()) continue;
                    //if (!FileHelper.IsExistFile(imgPath)) continue;

                    //var img = ImageHelper.GetThumbnailImageKeepRatio(new Bitmap(imgPath), 640, 360);// 424, 240);640, 360);848, 480

                    //也可以保存为输出流
                    using (MemoryStream ms = new MemoryStream())
                    using (ImageFactory fac = new ImageFactory())
                    {
                        fac.Load(imgPath)
                            .Resize(new Size(640, 360))
                            //.Quality(30)
                            .Overlay(new ImageProcessor.Imaging.ImageLayer
                            {
                                Image = new Bitmap("~/Content/logo.png"),
                                Size = new Size(133, 60),
                                Position = new Point(20, 295)
                            })
                            //.Rotate(90.0f)
                            .Save(ms);

                        using (Image img = System.Drawing.Image.FromStream(ms))
                        {
                            //Add the image to gifEncoder with default Quality
                            gifCreator.AddFrame(img, quality: GifQuality.Default);
                        } //Image disposed
                    }
                }
            }
        }
    }
}
