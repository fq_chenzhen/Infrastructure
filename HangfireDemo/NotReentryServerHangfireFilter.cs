﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Server;
using Infrastructure.Lib.Core;

namespace HangfireDemo
{
    public class NotReentryServerHangfireFilter : IServerFilter
    {
        /// 如果不希望在一个job还没执行完成的时候再次进入该job，添加如下过滤器：
        /// <summary>
        /// 判断job是否正在运行
        /// </summary>
        static ConcurrentDictionary<string, DateTime> JobRunnings = new ConcurrentDictionary<string, DateTime>();

        public NotReentryServerHangfireFilter()
        {
        }

        public void OnPerforming(PerformingContext filterContext)
        {
            var jobId = BuildJobId(filterContext.BackgroundJob);
            if (!JobRunnings.TryAdd(jobId, DateTime.Now))
            {
                filterContext.Canceled = true;
                return;
            }

            LogHelper.WriteLog(string.Format("{0} starting...", jobId));
        }

        public void OnPerformed(PerformedContext filterContext)
        {
            var jobId = BuildJobId(filterContext.BackgroundJob);
            DateTime tmp;
            JobRunnings.TryRemove(jobId, out tmp);
            LogHelper.WriteLog(string.Format("{0} finished.", jobId));
        }

        public string BuildJobId(BackgroundJob job)
        {
            return string.Format("{0}.{1}", job.Job.Type.FullName, job.Job.Method.Name);
        }
    }
}
