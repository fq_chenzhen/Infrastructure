﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.MemoryStorage;

namespace HangfireDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            GlobalConfiguration.Configuration
                .UseColouredConsoleLogProvider()
                .UseMemoryStorage();
            //                .UseSqlServerStorage(ConfigHelper.GetConnectionString("SqlConnStr"));

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });
            GlobalJobFilters.Filters.Add(new NotReentryServerHangfireFilter());


            BackgroundJob.Enqueue(() => Console.WriteLine("Fire-and-forget"));

            RecurringJob.AddOrUpdate(() => Console.WriteLine("Hello, world!"), "*/5 6-24 * * *", timeZone: TimeZoneInfo.Local);
            // 这是循环任务执行，"*/5 1-24 * * *"是CronExpression表达式，表示在1-24小时范围内每5分钟执行任务

            //服务端要加这行
            var _server = new BackgroundJobServer();
        }
    }
}
