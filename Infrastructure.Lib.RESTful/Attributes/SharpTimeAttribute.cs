﻿using System;
namespace Infrastructure.Lib.RESTful.Attributes
{
    /// <summary>
    /// 针对 DateTime 类型进行结束时间进行判断，向后归整小时处理
    /// Author：Jsg 2016.01.29
    /// </summary>
    public class SharpTimeAttribute : Attribute
    {
        //Range<DateTime>类型进行结束时间进行判断，向后归整小时处理
        //如：2016-01-29 14:25:21 处理之后变成 2016-01-29 15:00:00
    }
}
