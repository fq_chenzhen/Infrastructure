﻿using System;
namespace Infrastructure.Lib.RESTful.Attributes
{
    /// <summary>
    /// 执行url参数到方法参数的转换，主要针对url名称与方法参数名称不同的时候，比如方法参数是dataType,url则是data_type
    /// </summary>
    public class UNAttribute : Attribute
    {
        public string MappingName { get; set; }

        /// <summary>
        /// 指定在url上使用的参数名称
        /// </summary>
        /// <param name="mappingName"></param>
        public UNAttribute(string mappingName)
        {
            this.MappingName = mappingName;
        }
    }
}
