﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using Infrastructure.Lib.Core.Cache;
using Infrastructure.Lib.RESTful.Attributes;

namespace Infrastructure.Lib.RESTful.Extensions
{
    public static class HttpActionContextExtensions
    {
        /// <summary>
        /// 获取需要检查的必填参数信息
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        public static List<KeyValuePair<string, string>> GetUNMappingParameters(this HttpActionContext actionContext)
        {
            List<KeyValuePair<string, string>> mapping;
            Tuple<HttpMethod, string> request = new Tuple<HttpMethod, string>(actionContext.Request.Method, actionContext.Request.RequestUri.LocalPath);

            var key = string.Format("{0}_{1}", actionContext.Request.Method, actionContext.Request.RequestUri.LocalPath);
            mapping = CacheLayer.Get<List<KeyValuePair<string, string>>>(key);
            if (mapping==null)
            {
                mapping = new List<KeyValuePair<string, string>>();
                var result = actionContext.ActionDescriptor.GetParameters().Where(p => p.GetCustomAttributes<UNAttribute>().FirstOrDefault() != null).ToList();
                foreach (var item in result)
                {
                    var una = item.GetCustomAttributes<UNAttribute>().FirstOrDefault();
                    mapping.Add(new KeyValuePair<string, string>(item.ParameterName, una.MappingName));
                }

                //添加到缓存
                CacheLayer.Add(mapping, key);
            }

            return mapping;
        }
    }
}
