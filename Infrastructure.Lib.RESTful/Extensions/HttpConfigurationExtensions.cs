﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json;

namespace Infrastructure.Lib.RESTful.Extensions
{
    public static class HttpConfigurationExtensions
    {
        public static void ConfigureJsonSerializer(this HttpConfiguration config)
        {
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            //移除xml序列化器
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            settings.DateFormatString = "yyyy-MM-ddTHH:mm:ss";
            var jsi = new ApiResultContractResolver(JsonPropertyNameCase.Lower);
            //jsi.PropertyMatchMode = PropertyMatchMode.NameAndType;
            //jsi.ExcludeProperties.Add("id");
            settings.ContractResolver = jsi;
            config.Formatters.JsonFormatter.SerializerSettings = settings;
        }
    }
}
