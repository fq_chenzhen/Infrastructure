﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Infrastructure.Lib.RESTful.Middleware
{
    public static class OwinContextExtensions
    {
        public static string GetRequestID(this IOwinContext context)
        {
            return context.Get<string>("owin.RequestId").Replace("-", "");
        }

        public static void ReturnJson(this IOwinContext context, string json)
        {
            context.Response.Headers.Set("Content-Type", "application/json");
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public static void ReturnJson(this IOwinContext context, HttpConfiguration config, object model)
        {
            string json = JsonConvert.SerializeObject(model, config.Formatters.JsonFormatter.SerializerSettings);
            context.Response.Headers.Set("Content-Type", "application/json");
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public static JToken ReadBodyAsJson(this IOwinContext context)
        {
            return ReadBodyAsJsonAsync(context).Result;
        }

        public static async Task<JToken> ReadBodyAsJsonAsync(this IOwinContext context)
        {
            var body = await context.ReadBodyAsStringAsync();
            return body.ToJson();
        }

        public static JToken ToJson(this string jsonString)
        {
            if (jsonString != null && jsonString.StartsWith("["))
            {
                return JsonConvert.DeserializeObject<JArray>(jsonString);
            }

            return JsonConvert.DeserializeObject<JObject>(jsonString);
        }

        public static string ReadBodyAsString(this IOwinContext context)
        {
            return context.ReadBodyAsStringAsync().Result;
        }

        public static async Task<string> ReadBodyAsStringAsync(this IOwinContext context)
        {
            var sb = new StringBuilder();
            var buffer = new byte[8000];
            var read = 0;

            read = await context.Request.Body.ReadAsync(buffer, 0, buffer.Length);
            while (read > 0)
            {
                sb.Append(Encoding.UTF8.GetString(buffer));
                buffer = new byte[8000];
                read = await context.Request.Body.ReadAsync(buffer, 0, buffer.Length);
            }

            return sb.ToString();
        }

        public static async Task<IFormCollection> FormAsync(this IOwinContext context)
        {
            return await context.Request.ReadFormAsync();
        }

        public static IFormCollection Form(this IOwinContext context)
        {
            return context.FormAsync().Result;
        }

        /// <summary>
        /// 日志.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="message">The message.</param>
        /// <param name="appendTime">if set to <c>true</c> [append time].</param>
        /// <param name="arg">The argument.</param>
        public static void Log(this IOwinContext context, string message = "", bool appendTime = true, params object[] arg)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                context.TraceOutput.WriteLine("");
            }
            else
            {
                if (appendTime)
                {
                    context.TraceOutput.WriteLine( DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff]") + " " + message, arg);
                }
                else
                {
                    context.TraceOutput.WriteLine(message, arg);
                }
            }
        }
    }
}
