﻿/************************************************************************************************************
* file    : apiresultcontractresolver.cs
* author  : YLH-PC
* function: 新的解析约定，用于将json的key转换为小写
* history : created by YLH-PC 08/10/2016, 18:28
************************************************************************************************************/
using Newtonsoft.Json.Serialization;
using System.Reflection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Globalization;

namespace Infrastructure.Lib.RESTful.Extensions
{
    /// <summary>
    /// ApiResult序列化器，对Json属性名称的转换处理
    /// </summary>
    /// <seealso cref="Newtonsoft.Json.Serialization.DefaultContractResolver" />
    public class ApiResultContractResolver : DefaultContractResolver
    {

        /// <summary>
        /// The _option
        /// </summary>
        public readonly JsonPropertyNameCase _propertyNameCaseType;


        /// <summary>
        /// Initializes a new instance of the <see cref="ApiResultContractResolver"/> class.
        /// </summary>
        /// <param name="option">The option.</param>
        public ApiResultContractResolver(JsonPropertyNameCase option)
        {
            _propertyNameCaseType = option;
        }

        /// <summary>
        /// Resolves the name of the property.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns>Resolved name of the property.</returns>
        protected override string ResolvePropertyName(string propertyName)
        {
            switch (_propertyNameCaseType)
            {
                case JsonPropertyNameCase.Lower:
                    return propertyName.ToLower();
                case JsonPropertyNameCase.Upper:
                    return propertyName.ToUpper();
                case JsonPropertyNameCase.Original:
                default:
                    return propertyName;
            }
        }
    }
}
