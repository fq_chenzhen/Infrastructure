﻿using System.Linq;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using Infrastructure.Lib.Core;
using Infrastructure.Lib.RESTful.Extensions;

/************************************************************************************************************
* author 	:  ylh
* email  	:  ylh@strongsoft.net 
* function	:  NamedList类型转换器
* history	:  created by ylh 3/13/2014 9:58:42 AM 
************************************************************************************************************/
namespace Infrastructure.Lib.RESTful.ModelBinders
{
    /// <summary>
    /// NamedList类型转换器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NamedListModelBinder<T> : IModelBinder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var modelName = bindingContext.ModelName;
            var unMapping = actionContext.GetUNMappingParameters().FirstOrDefault(t => t.Key == bindingContext.ModelName);
            if (unMapping.Key != null) { modelName = unMapping.Value; }
            ValueProviderResult result = bindingContext.ValueProvider.GetValue(modelName);
            if (result != null)
            {
                NamedList<T> list = result.AttemptedValue;
                bindingContext.Model = list;

                return true;
            }
            return false;
        }
    }
}
