﻿using System;
using System.Linq;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using Infrastructure.Lib.RESTful.Extensions;
using Infrastructure.Lib.Core;

/************************************************************************************************************
* author 	:  ylh
* email  	:  ylh@strongsoft.net 
* function	:  Range类型转换器
* history	:  created by ylh 3/13/2014 9:58:42 AM 
************************************************************************************************************/
namespace Infrastructure.Lib.RESTful.ModelBinders
{
    /// <summary>
    /// Range类型转换器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RangeModelBinder<T> : IModelBinder where T : IComparable<T>, IEquatable<T>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var modelName = bindingContext.ModelName;
            var unMapping = actionContext.GetUNMappingParameters().FirstOrDefault(t => t.Key == bindingContext.ModelName);
            if (unMapping.Key != null) { modelName = unMapping.Value; }
            ValueProviderResult result = bindingContext.ValueProvider.GetValue(modelName);

            //未设置和启用模拟时间，检查url是否有传值
            if (result != null)
            {
                bindingContext.Model = new Range<T>(result.AttemptedValue);
                return true;
            }

            return false;
        }
    }
}
