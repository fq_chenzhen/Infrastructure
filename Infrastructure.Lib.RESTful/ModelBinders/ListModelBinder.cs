﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using Infrastructure.Lib.Core;
using Infrastructure.Lib.RESTful.Extensions;

/************************************************************************************************************
* author 	:  ylh
* email  	:  ylh@strongsoft.net 
* function	:  List类型转换器
* history	:  created by ylh 3/13/2014 9:58:42 AM 
************************************************************************************************************/
namespace Infrastructure.Lib.RESTful.ModelBinders
{
    /// <summary>
    /// List类型转换器
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListModelBinder<T> : IModelBinder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var modelName = bindingContext.ModelName;
            var unMapping = actionContext.GetUNMappingParameters().FirstOrDefault(t => t.Key == bindingContext.ModelName);
            if (unMapping.Key != null) { modelName = unMapping.Value; }
            ValueProviderResult result = bindingContext.ValueProvider.GetValue(modelName);
            if (result != null)
            {
                if (typeof(T).Name.Contains("NamedList"))
                {
                    Regex regex = new Regex("],");
                    string newAttemptedValue = regex.Replace(result.AttemptedValue, "]+");
                    string[] filters = newAttemptedValue.Split('+');
                    if (filters.Length >= 1)
                    {
                        List<NamedList<string>> tempResult = new List<NamedList<string>>();
                        foreach (string item in filters)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                NamedList<string> list = item;
                                tempResult.Add(list);
                            }
                        }

                        bindingContext.Model = tempResult;
                        return true;
                    }
                }
                else if (typeof(T).Name.Contains("Range"))
                {
                    Regex regex = new Regex("],");
                    string newAttemptedValue = regex.Replace(result.AttemptedValue, "]+");
                    string[] filters = newAttemptedValue.Split('+');
                    if (filters.Length >= 1)
                    {
                        List<Range<decimal>> tempResult = new List<Range<decimal>>();
                        foreach (string item in filters)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                Range<decimal> list = item;
                                tempResult.Add(list);
                            }
                        }

                        bindingContext.Model = tempResult;
                        return true;
                    }
                }
                else
                {
                    string[] valueList = result.AttemptedValue.Split(',');
                    bindingContext.Model = Array.ConvertAll<string, T>(valueList, new Converter<string, T>(Convert<T>)).ToList();
                    return true;
                }
            }
            //else
            //{
            //    bindingContext.Model = new List<T>();
            //    return true;
            //}
            return false;
        }

        private T Convert<T1>(string input)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter != null)
            {
                return (T)converter.ConvertFromString(input);
            }
            return default(T);
        }
    }
}
