﻿using System;

public class UploadController
{
	 [HttpPost]
        public async Task<IActionResult> UploadPicture(long? DeviceProduceId)
        {
            try
            {
                var pictureFile = Request.Form.Files.First();

                //Check input
                if (pictureFile == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }

                if (pictureFile.Length > MaxPictureSize)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit", AppConsts.MaxProfilPictureBytesUserFriendlyValue));
                }

                var fileDir = Path.Combine(_env.WebRootPath, "UploadFiles");
                if (!Directory.Exists(fileDir))
                {
                    Directory.CreateDirectory(fileDir);
                }

                var fileName = ContentDispositionHeaderValue
                    .Parse(pictureFile.ContentDisposition)
                    .FileName
                    .Value
                    .Trim('"');

                var webPath = $"/UploadFiles/{fileName}";

                // 获取后缀
                var extension = Path.GetExtension(fileName);

                var size = pictureFile.Length;

                string filePath = Path.Combine(fileDir, fileName);

                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    pictureFile.CopyTo(fs);
                    fs.Flush();
                }

                StationPicturesEditDto stationPicturesEditDto = new StationPicturesEditDto()
                {
                    DeviceProduceId = DeviceProduceId,
                    PicturePath = filePath,
                    WebPath = webPath
                };

                var resultDto = await _stationPicturesApplicationService.CreateOrUpdate(
                    new CreateOrUpdateStationPicturesInput { StationPictures = stationPicturesEditDto });

                return Json(resultDto);
            }
            catch (UserFriendlyException ex)
            {
                return new JsonResult(new ErrorInfo(ex.Message));
            }
        }
}
