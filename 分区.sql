﻿CREATE PARTITION FUNCTION partfunPic (int)   
AS RANGE RIGHT FOR VALUES (500000,1000000,1500000,2000000,2500000,3000000,3500000,4000000,4500000,5000000)

CREATE PARTITION SCHEME partschSalePic  
AS PARTITION partfunPic
TO (   
    water_1,   
    water_2,   
    water_3,   
    water_4,   
    water_5,
    water_6,
    water_7,
    water_8,
    water_9,
    water_10,
    [PRIMARY])
    
-- CREATE TABLE [dbo].[PictureDiagnosis](
	-- [id] [int] NOT NULL,
	-- [stcd] [varchar](10) NULL,
	-- [srcPath] [nvarchar](500) NULL,
	-- [destPath] [nvarchar](500) NULL,
	-- [waterLevel] [decimal](10, 2) NULL,
	-- [procName] [varchar](30) NULL,
	-- [picDate] [datetime] NULL,
	-- [creDate] [datetime] NULL 
-- ) ON partschSalePic ( id )

GO