﻿using Autofac;
using Autofac.Extras.DynamicProxy2;
using System.Reflection;

namespace AutofacAopDemo
{
    public class AutofacRegister
    {
        public static void Register(ContainerBuilder b)
        {
            Castle.DynamicProxy.Generators.AttributesToAvoidReplicating.Add(typeof(System.Security.Permissions.UIPermissionAttribute));

            //b.RegisterType<AutoLockIInterceptor>().SingleInstance();
            //b.RegisterType<AuthorizationInterceptor>().SingleInstance();            
            //b.RegisterType<FrmActionLock>().InstancePerDependency();
            //b.RegisterType<FrmAuth>().InstancePerDependency();

            b.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(type => typeof(IDependency).IsAssignableFrom(type) && !type.GetTypeInfo().IsAbstract)
                .EnableClassInterceptors() //.InstancePerLifetimeScope()
                .InterceptedBy(typeof(AutoLockIInterceptor))
                //.InterceptedBy(typeof(AuthorizationInterceptor))
                ;
        }
    }
}
