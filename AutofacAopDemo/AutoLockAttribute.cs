﻿using Infrastructure.Lib.Core;
using System;

namespace AutofacAopDemo
{
    [AttributeUsage(AttributeTargets.All, Inherited = true)]
    public class AutoLockAttribute : Attribute
    {
        /// <summary>
        /// 超时时间秒
        /// </summary>
        public int Timeout { get; set; }

        public string Password { get; set; }

        public AutoLockAttribute()
        {
            Timeout = 60;
            Password = ConfigHelper.GetAppSetting("AutoLockPassword") ?? HashHelper.GetMd5("strong");
        }
    }
}
