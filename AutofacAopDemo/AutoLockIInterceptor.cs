﻿using Castle.DynamicProxy;
//using log4net;
using System;
using System.Linq;
using System.Reflection;

namespace AutofacAopDemo
{
    public class AutoLockIInterceptor : IInterceptor
    {
        //可自行实现日志器，此处可忽略
        /// <summary>
        /// 日志记录器
        /// </summary>
        //private static readonly ILog Logger = LogManager.GetLogger(typeof(AutoLockIInterceptor));

        // 是否开发模式
        private bool isDev = false;
        public void Intercept(IInvocation invocation)
        {
            if (!isDev && Environment.UserInteractive)
            {
                //没锁住直接运行
                //if (!Configuration.AutoLocked)
                //{
                //    invocation.Proceed();
                //    return;
                //}

                MethodInfo methodInfo = invocation.MethodInvocationTarget;
                if (methodInfo == null)
                {
                    methodInfo = invocation.Method;
                }

                AutoLockAttribute autoLock = methodInfo.GetCustomAttributes<AutoLockAttribute>(true).FirstOrDefault();
                if (autoLock != null)
                {
                    try
                    {
                        //时间到自动锁住，判断是否输入正确密码
                        //FrmActionLock lockFrm = ContainerManager.Resolve<FrmActionLock>();// new FrmActionLock();
                        //lockFrm.Password = autoLock.Password;

                        //if (lockFrm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        //{
                            ////解锁
                            //Configuration.AutoLocked = false;
                            ////设置下次自动上锁住时间
                            //Configuration.NextAutoLockDateTime = DateTime.Now.AddSeconds(autoLock.Timeout);
                            invocation.Proceed();
                        //}
                        //else
                        //{
                        //    //通过判断否包含关闭参数，检查当前操作是否是关闭操作，
                        //    //var e = invocation.Arguments.Where(o => typeof(System.Windows.Forms.FormClosingEventArgs).IsAssignableFrom(o.GetType())).FirstOrDefault();
                        //    //if (null != e)
                        //    //{
                        //    //    //如果是关闭操作不退出
                        //    //    (e as System.Windows.Forms.FormClosingEventArgs).Cancel = true;
                        //    //}
                        //}
                    }
                    catch (Exception ex)
                    {
                        // 记录异常
                        //Logger.Error(ex.Message);
                    }
                }
                else
                {
                    invocation.Proceed();
                }
            }
            else
            {
                // 开发模式直接跳过拦截
                invocation.Proceed();
            }
        }
    }
}
