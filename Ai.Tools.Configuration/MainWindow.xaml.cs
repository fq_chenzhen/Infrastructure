﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Infrastructure.Lib.Core;

namespace Ai.Tools.Configuration
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Global
            InitializeComponent();
        }

        private void BtnEncrypted_Click(object sender, RoutedEventArgs e)
        {
            var str = TbSource.Text.Trim();
            if(string.IsNullOrEmpty(str)) return;
            TbEncrypted.Text = DesHelper.Encrypt(str, "Strong9889gnorts1234%^&*");

            TbEncrypted.Focus();
            TbEncrypted.SelectAll();
            Clipboard.SetDataObject(TbEncrypted.Text.Trim());
        }

        private void BtnDecrypted_Click(object sender, RoutedEventArgs e)
        {
            var str = TbEncrypted.Text.Trim();
            if (string.IsNullOrEmpty(str)) return;
            TbSource.Text = DesHelper.Decrypt(str, "Strong9889gnorts1234%^&*");

            TbSource.Focus();
            TbSource.SelectAll();
            Clipboard.SetDataObject(TbSource.Text.Trim());
        }

        private void BtnCopySource_Click(object sender, RoutedEventArgs e)
        {
            TbSource.Focus();
            TbSource.SelectAll();
            Clipboard.SetText(TbSource.Text.Trim());
        }

        private void BtnCopyEncrypted_Click(object sender, RoutedEventArgs e)
        {
            TbEncrypted.Focus();
            TbEncrypted.SelectAll();
            Clipboard.SetText(TbEncrypted.Text.Trim());
        }
    }
}
