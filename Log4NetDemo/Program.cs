﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Lib.Core;
using log4net;

namespace Log4NetDemo
{
    class Program
    {

        // 注意在assembly.cs里添加：[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.config", Watch = true)]
        // 在 Infrastructure.Lib.Core 的 assembly.cs 中也需要配置

        static void Main(string[] args)
        {
            try
            {
                LogHelper.WriteLog("xx是个大坏蛋");

                try
                {
                    //todo
                    throw new NullReferenceException();
                }
                catch (Exception ex)
                {
                    LogHelper.WriteLog("被除数为零，呃呃呃，小学数学没学好！", ex);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }
    }
}
