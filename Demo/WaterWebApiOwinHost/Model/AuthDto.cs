﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterWebApiOwinHost.Model
{
    public class AuthDto
    {
        public int _id { get; set; }

        public string cpuId { get; set; }

        public string platform { get; set; }

        public int timeSpan { get; set; }
    }
}
