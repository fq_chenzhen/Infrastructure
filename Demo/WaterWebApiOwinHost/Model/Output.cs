﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WaterWebApiOwinHost.Model
{
    public interface IOutput
    {
        string Write(string message);
    }

    public class Output : IOutput
    {
        public string Write(string message)
        {
            return message;
        }
    }

}