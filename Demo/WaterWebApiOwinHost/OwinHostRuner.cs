﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Lib.Core;

namespace WaterWebApiOwinHost
{
    public class OwinHostRuner
    {
        public OwinHostRuner()
        { }

        public void Start()
        {
            string[] urls = { string.Format("{0}:{1}", ConfigHelper.GetAppSetting("url"), ConfigHelper.GetAppSetting("port")) };
            StartOptions options = new StartOptions();
            foreach (var url in urls)
            {
                options.Urls.Add(url);
            }

            using (WebApp.Start<Startup>(options))
            {
                try
                {
                    Console.WriteLine("已绑定的地址：");
                    for (int i = 0; i < options.Urls.Count; i++)
                    {
                        Console.WriteLine("({0}) {1}", (i + 1), options.Urls[i]);
                    }

                    Console.WriteLine("Press [enter] to quit...");
                    Console.ReadLine();

                    while (true)
                    {
                        var x = Console.ReadLine();
                        if (x == "quit") break;
                        Thread.Sleep(1000);
                    }

                }
                catch (Exception ex)
                {
                    WriteErrorLog(ex.InnerException != null ? ex.InnerException : ex);
                }
            }
        }
        public void Stop()
        {

        }

        /// <summary>
        /// 写异常日志
        /// </summary>
        /// <param name="ex"></param>
        private static void WriteErrorLog(Exception ex)
        {
            Console.WriteLine("========================================================");
            Console.WriteLine("详细信息：{0}", ex.ToString());
            Console.WriteLine("========================================================");
        }
    }
}
