﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterWebApiOwinHost.Middleware
{
    public class DefaultPageMiddleware : OwinMiddleware
    {

        public DefaultPageMiddleware(OwinMiddleware next)
            : base(next)
        {
        }

        public async override Task Invoke(IOwinContext context)
        {
            var path = context.Request.Path.Value;
            if (!path.Equals("/") && !path.ToLower().Equals("/index.html"))
            {
                var startTime = DateTime.Now;
                //context.Log();
                // context.Log("[{0}] 开始:{1} {2} ", appendTime: false, arg: new object[] { startTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), context.Request.Method, context.Request.Path.Value });
                await Next.Invoke(context);
            }
            else
            {
                await Task.Run(() =>
                {
                    context.Response.Redirect("/Public/Html/Index.html");
                });
            }
        }
    }
}
