﻿using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterWebApiOwinHost.Middleware
{
    public static class MiddlewareExtentions
    {
        public static void UseDefaultPageMiddleware(this IAppBuilder app)
        {
            app.Use<DefaultPageMiddleware>();
        }
    }
}
