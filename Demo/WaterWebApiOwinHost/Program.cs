﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;
using Infrastructure.Lib.Core;

namespace WaterWebApiOwinHost
{
    class Program
    {
        static void Main(string[] args)
        {
            // change from service account's dir to more logical one
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            HostFactory.Run(x =>
            {
                x.Service<OwinHostRuner>(s =>                        //2
                {
                    s.ConstructUsing(name => new OwinHostRuner());     //3
                    s.WhenStarted(tc => Task.Run(() => tc.Start()));              //4
                    s.WhenStopped(tc => tc.Stop());               //5
                });

                x.RunAsLocalSystem();

                x.StartAutomaticallyDelayed();// 自动（延迟启动）

                x.SetDescription(ConfigHelper.GetAppSetting("serviceDescription"));        //7
                x.SetDisplayName(ConfigHelper.GetAppSetting("serviceDisplayName"));                       //8
                x.SetServiceName(ConfigHelper.GetAppSetting("serviceName"));                       //9
            });
        }
    }
}
