﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Infrastructure.Lib.Core;
using DataProvider;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Net.Http.Headers;

namespace WaterWebApiOwinHost.Api
{
    public class PicturesController : ApiController
    {
        private IPictureDiagnosisService _picDiagnosisService;

        public PicturesController(IPictureDiagnosisService picDiagnosisService)
        {
            _picDiagnosisService = picDiagnosisService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Data(string stcd, Range<DateTime> time)
        {
            var result = await _picDiagnosisService.QueryTimeRangeAsync(stcd, time);
            return Json(result);
        }

        [HttpGet]
        public async Task<IHttpActionResult> LineChartData(string stcd, Range<DateTime> time)
        {
            var returnDatas = await _picDiagnosisService.QueryTimeRangeAsync(stcd, time);

            dynamic data = new System.Dynamic.ExpandoObject();
            var categories = (from d in returnDatas select new { picDate = ((DateTime)d.picDate).ToString("yyyy-MM-dd HH:mm:ss") }).ToArray();
            var catLists = new List<string>();
            categories.ForEach(c =>
            {
                catLists.Add(c.picDate);
            });

            data.categories = catLists;
            data.data = from d in returnDatas
                        select new
                        {
                            picDate = ((DateTime)d.picDate).ToString("yyyy-MM-dd HH:mm:ss"),
                            value = d.waterRulerLevelCm,
                            waterActualLevelM = d.waterActualLevelM,
                            destPath = d.destPath
                        };
            return Json(data);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Image(string imgPath)
        {
            var imgByte = await Task.Run(() =>
            {
                //从图片中读取byte   
                var imgByte1 = File.ReadAllBytes(imgPath);
                return imgByte1;
                //从图片中读取流   
                //var imgStream = new MemoryStream(File.ReadAllBytes(imgPath));
            });

            var resp = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(imgByte)
                //或者   
                //Content = new StreamContent(stream)   
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            return resp;


        }
    }
}
