﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WaterWebApiOwinHost.Api
{
    public class ValuesController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Json(new DateTime[] { System.DateTime.Now.AddDays(-1), System.DateTime.Now }, new Newtonsoft.Json.JsonSerializerSettings { DateFormatString = "yyy-MM-dd HH:mm:ss" });
        }

        public string Get(int id)
        {
            return "value";
        }

        public void Post([FromBody]string value)
        {

        }

        public void Put(int id, [FromBody]string value)
        {

        }

        public void Delete(int id)
        {

        }
    }
}
