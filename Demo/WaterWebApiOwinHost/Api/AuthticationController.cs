﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Infrastructure.Lib.Core;
using DataProvider;
using WaterWebApiOwinHost.Model;
using Newtonsoft.Json;

namespace WaterWebApiOwinHost.Api
{
    public class AuthticationController : ApiController
    {
        private IPicturePlatformService _picPlatformService;
        private IPictureAuthorizeService _picAuthorizeService;

        public AuthticationController(IPicturePlatformService picPlatformService, IPictureAuthorizeService picAuthorizeService)
        {
            _picPlatformService = picPlatformService;
            _picAuthorizeService = picAuthorizeService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Platforms()
        {
            var result = await _picPlatformService.GetAllAsync();
            return Json(result, new JsonSerializerSettings { DateFormatString = "yyy-MM-dd HH:mm:ss" });
        }

        [HttpGet]
        public async Task<IHttpActionResult> All()
        {
            return Json(await _picAuthorizeService.GetAllAsync()
                , new JsonSerializerSettings { DateFormatString = "yyy-MM-dd HH:mm:ss" });
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get(string _id)
        {
            var model = await _picAuthorizeService.GetAsync(Convert.ToInt32(_id));
            var returnDatas = model != null ? new List<PictureAuthorizeModel> { model } : null;
            return Json(returnDatas, new JsonSerializerSettings { DateFormatString = "yyy-MM-dd HH:mm:ss" });
        }

        [HttpGet]
        public async Task<IHttpActionResult> Search(string value)
        {
            return Json(await _picAuthorizeService.Search(value)
                , new JsonSerializerSettings { DateFormatString = "yyy-MM-dd HH:mm:ss" });
        }

        [HttpGet]
        public async Task<IHttpActionResult> AuthCode(string _id)
        {
            dynamic returnDatas = new System.Dynamic.ExpandoObject();
            var amodel = await _picAuthorizeService.GetAsync(_id);
            returnDatas.cpuId = amodel.cpuId;
            returnDatas.platform = amodel.platform;
            returnDatas.timeSpan = amodel.timeSpan;
            returnDatas.authCode = _picAuthorizeService.Encrypt(amodel.ObjectToJson());

            return Json(returnDatas, new JsonSerializerSettings { DateFormatString = "yyy-MM-dd HH:mm:ss" });
        }

        public async Task<IHttpActionResult> Post([FromBody]AuthDto dto)
        {
            var iModel = dto.MapTo<PictureAuthorizeModel>();
            iModel.creDate = DateTime.Now;
            var result = await _picAuthorizeService.InsertAsync(iModel);

            if (result > 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        public async Task<IHttpActionResult> Put([FromBody]AuthDto dto)
        {
            var uModel = dto.MapTo<PictureAuthorizeModel>();
            uModel.creDate = DateTime.Now;
            var result = await _picAuthorizeService.UpdateAsync(uModel);

            if (result == null)
            {
                return BadRequest();
            }
            else
            {
                return Ok();
            };
        }


        public async Task<IHttpActionResult> Delete(int _id)
        {
            var result = await _picAuthorizeService.DeleteAsync(new PictureAuthorizeModel { id = _id });

            if (result)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

    }
}
