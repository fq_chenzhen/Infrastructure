﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WaterWebApiOwinHost.Model;

namespace WaterWebApiOwinHost.Api
{
    public class OutputController : ApiController
    {
        private IOutput writer;

        public OutputController(IOutput writer)
        {
            this.writer = writer;
        }

        [HttpGet]
        public string Write(string msg)
        {
            return this.writer.Write(msg);
        }
    }
}
