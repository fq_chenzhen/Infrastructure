﻿using DataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Infrastructure.Data.Layer;

namespace WaterWebApiOwinHost.Api
{
    public class StationsController : ApiController
    {
        private IPictureStConfigService _picConfigService;

        public StationsController(IPictureStConfigService picConfigService)
        {
            _picConfigService = picConfigService;
        }

        public async Task<IHttpActionResult> Get()
        {
            var lists = await _picConfigService.GetAllAsync();
            if (lists != null)
            {
                return Json(lists.Select(p => new
               {
                   stcd = p.stcd,
                   stname = p.stname,
                   use = p.use,
                   baseValue = p.baseValue,
                   rulerLen = p.rulerLen,
                   srcDir = p.srcDir,
                   saveDir = p.saveDir,
                   type = p.type,
                   param = p.param.Split('|'),
                   roi = p.roi.Split('|'),
                   isJumpProc = p.isJumpProc,
                   jumpValue = p.jumpValue
               }));
            }

            return NotFound();
        }

        public async Task<IHttpActionResult> Get(bool isUse)
        {
            var lists = await _picConfigService.GetAllAsync();
            if (isUse)
            {
                return Json(lists.Where(c => (bool)c.use).Select(c => new { c.stcd, c.stname }));
            }

            return NotFound();
        }
    }
}
