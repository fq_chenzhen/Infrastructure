﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Configuration;
using Autofac.Integration.WebApi;
using Autofac.Integration.Owin;
using Microsoft.Owin;
using Owin;
using System.Reflection;
using System.Web.Http;
using WaterWebApiOwinHost.Model;
using Infrastructure.Lib.RESTful.Extensions;
using System.IO;
using Infrastructure.Lib.Core;
using Infrastructure.Lib.RESTful.ModelBinders;
using System.Web.Http.Cors;
using AutoMapper;
using DataProvider;
using WaterWebApiOwinHost.Middleware;
using Newtonsoft.Json;

[assembly: OwinStartup(typeof(WaterWebApiOwinHost.Startup))]

namespace WaterWebApiOwinHost
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);
            var builder = new ContainerBuilder();
            //builder.RegisterType<Output>().As<IOutput>();
            // Register dependencies, then...
            IOCRegister(builder);

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiModelBinderProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();

            app.UseAutofacMiddleware(container);

            HttpConfiguration config = new HttpConfiguration();
            //跨域配置
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            config.ConfigureJsonSerializer();

            BindParameterTypes(config);

            RegisterRoutes(config);

            InitMapper();            

            app.UseDefaultPageMiddleware();

            app.UseErrorPage();

            app.UseStaticFiles();

            app.UseAutofacWebApi(config);

            app.UseWebApi(config);
        }

        private static void InitMapper()
        {
            Mapper.Initialize(m =>
            {
                m.CreateMap<AuthDto, PictureAuthorizeModel>()
                    .ForMember(p => p.id, opt => opt.MapFrom(a => a._id));
            });
        }

        /// <summary>
        /// 绑定参数类型
        /// </summary>
        /// <param name="config"></param>
        private static void BindParameterTypes(HttpConfiguration config)
        {
            //通用自定义参数类型，用于URL参数的转换映射
            config.BindParameter(typeof(List<string>), new ListModelBinder<string>());
            config.BindParameter(typeof(List<int>), new ListModelBinder<int>());
            config.BindParameter(typeof(List<long>), new ListModelBinder<long>());
            config.BindParameter(typeof(List<double>), new ListModelBinder<double>());
            config.BindParameter(typeof(List<float>), new ListModelBinder<float>());
            config.BindParameter(typeof(List<NamedList<string>>), new ListModelBinder<NamedList<string>>());
            config.BindParameter(typeof(Range<DateTime>), new RangeModelBinder<DateTime>());
            config.BindParameter(typeof(Range<int>), new RangeModelBinder<int>());
            config.BindParameter(typeof(Range<double>), new RangeModelBinder<double>());
            config.BindParameter(typeof(Range<decimal>), new RangeModelBinder<decimal>());
            config.BindParameter(typeof(NamedList<string>), new NamedListModelBinder<string>());
            config.BindParameter(typeof(Dictionary<string, string>), new DicModelBinder<Dictionary<string, string>>());
        }

        /// <summary>
        /// 注册路由
        /// </summary>
        /// <param name="config"></param>
        private static void RegisterRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "MvcStyleApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { action = RouteParameter.Optional, id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "Api",
               routeTemplate: "api/{controller}/{id}",
               defaults: new { action = RouteParameter.Optional, id = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(name: "Error404", routeTemplate: "{*url}", defaults: new { controller = "Error", action = "Handle404" });
        }

        /// <summary>
        /// 注册IOC
        /// </summary>
        /// <param name="builder"></param>
        private static void IOCRegister(ContainerBuilder builder)
        {
            //XmlConfigurator.Configure(new FileInfo(Path.Combine(HttpContext.Current.Server.MapPath("/"), "log4net.config")));
            //var logger = LogManager.GetLogger("");
            //builder.RegisterInstance(logger).As<ILog>().SingleInstance();


            var configPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\Config");

            builder.RegisterModule(new ConfigurationSettingsReader("components", Path.Combine(configPath, "components.xml")));
            //更多配置文件.....
        }
    }
}