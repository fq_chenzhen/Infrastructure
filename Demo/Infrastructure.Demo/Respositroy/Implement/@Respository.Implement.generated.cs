﻿using Infrastructure.Data.Layer;

namespace Infrastructure.Demo
{
	public partial class PictureDiagnosisRepository : BaseRespository<PictureDiagnosisModel>, IPictureDiagnosisRepository { }

	public partial class PictureStConfigRepository : BaseRespository<PictureStConfigModel>, IPictureStConfigRepository { }

}
