﻿using Infrastructure.Data.Layer;

namespace Infrastructure.Demo
{
	public partial interface IPictureDiagnosisRepository : IBaseRespository<PictureDiagnosisModel> { }

	public partial interface IPictureStConfigRepository : IBaseRespository<PictureStConfigModel> { }

}
