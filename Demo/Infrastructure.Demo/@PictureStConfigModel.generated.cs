﻿using System;
using Dapper.Contrib.Extensions;

namespace Infrastructure.Demo
{
    [Table("PictureStConfig")]
	public partial class PictureStConfigModel
    {
        /// <summary>
        /// 站点编号
        /// </summary>        
        [ExplicitKey]
        public string stcd { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>        
        public string stname { get; set; }

        /// <summary>
        /// 是否使用
        /// </summary>        
        public bool? use { get; set; }

        /// <summary>
        /// 水位基值
        /// </summary>        
        public double? baseValue { get; set; }

        /// <summary>
        /// 水尺量程
        /// </summary>        
        public int? rulerLen { get; set; }

        /// <summary>
        /// 源路径
        /// </summary>        
        public string srcDir { get; set; }

        /// <summary>
        /// 保存路径
        /// </summary>        
        public string saveDir { get; set; }

        /// <summary>
        /// 类型
        /// </summary>        
        public string type { get; set; }

        /// <summary>
        /// 水尺参数
        /// </summary>        
        public string param { get; set; }

        /// <summary>
        /// 水尺roi区域
        /// </summary>        
        public string roi { get; set; }

    }
}
