﻿using Infrastructure.Data.Layer;

namespace Infrastructure.Demo
{
	public partial interface IPictureDiagnosisService : IBaseService<PictureDiagnosisModel> { }

	public partial interface IPictureStConfigService : IBaseService<PictureStConfigModel> { }

}
