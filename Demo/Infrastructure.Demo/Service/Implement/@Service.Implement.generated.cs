﻿using Infrastructure.Data.Layer;

namespace Infrastructure.Demo
{
	public partial class PictureDiagnosisService : BaseService<PictureDiagnosisModel>, IPictureDiagnosisService 
    {
        public PictureDiagnosisService(IPictureDiagnosisRepository reps)
        {
            modelDal = reps;
        }
    }

	public partial class PictureStConfigService : BaseService<PictureStConfigModel>, IPictureStConfigService 
    {
        public PictureStConfigService(IPictureStConfigRepository reps)
        {
            modelDal = reps;
        }
    }

}
