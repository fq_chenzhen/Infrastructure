﻿using System;
using Dapper.Contrib.Extensions;

namespace Infrastructure.Demo
{
    [Table("PictureDiagnosis")]
	public partial class PictureDiagnosisModel
    {
        /// <summary>
        /// id
        /// </summary>        
        [Key]
        public int id { get; set; }

        /// <summary>
        /// 站点编号
        /// </summary>        
        public string stcd { get; set; }

        /// <summary>
        /// src
        /// </summary>        
        public string srcPath { get; set; }

        /// <summary>
        /// dest
        /// </summary>        
        public string destPath { get; set; }

        /// <summary>
        /// 水尺水位值
        /// </summary>        
        public decimal? waterRulerLevelCm { get; set; }

        /// <summary>
        /// 实际水位值
        /// </summary>        
        public decimal? waterActualLevelM { get; set; }

        /// <summary>
        /// 算法名称
        /// </summary>        
        public string procName { get; set; }

        /// <summary>
        /// 识别时间
        /// </summary>        
        public DateTime? picDate { get; set; }

        /// <summary>
        /// 增加时间
        /// </summary>        
        public DateTime? creDate { get; set; }

    }
}
