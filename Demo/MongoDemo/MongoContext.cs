﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDemo
{
    public class MongoContext
    {
        private IMongoDatabase dataBase;

        public IMongoCollection<ContactModel> Contacts
        {
            get
            {
                return dataBase.GetCollection<ContactModel>("contacts");
            }
        }

        public MongoContext()
        {
            var builder = new MongoUrlBuilder("mongodb://112.74.198.149:27017");
            var url = builder.ToMongoUrl();
            var settings = MongoClientSettings.FromUrl(url);
            settings.Credential = MongoCredential.CreateCredential("website", "strong", "gnorts9889");
            var client = new MongoClient(settings);
            dataBase = client.GetDatabase("website");
        }
    }
}
