﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDemo
{
    public class ContactModel
    {
        public string CompanyName { get; set; }

        public string UserName { get; set; }

        public long PhoneNum { get; set; }

        public string Email { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
