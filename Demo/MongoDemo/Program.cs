﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MongoDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var context =  new MongoContext();

            context.Contacts.InsertOneAsync(new ContactModel { });

            context.Contacts.AsQueryable().OrderBy(c => c.CreationTime).ToList();

            //var client = new MongoClient("mongodb://localhost:27017");
            //var database = client.GetDatabase("foo");
            //var collection = database.GetCollection<BsonDocument>("bar");

            //await collection.InsertOneAsync(new BsonDocument("Name", "Jack"));

            //var list = await collection.Find(new BsonDocument("Name", "Jack"))
            //    .ToListAsync();

            //foreach (var document in list)
            //{
            //    Console.WriteLine(document["Name"]);
            //}


            //using MongoDB.Bson;
            //using MongoDB.Driver;
            //public class Person
            //{
            //    public ObjectId Id { get; set; }
            //    public string Name { get; set; }
            //}
            //var client = new MongoClient("mongodb://localhost:27017");
            //var database = client.GetDatabase("foo");
            //var collection = database.GetCollection<Person>("bar");

            //await collection.InsertOneAsync(new Person { Name = "Jack" });

            //var list = await collection.Find(x => x.Name == "Jack")
            //    .ToListAsync();

            //foreach(var person in list)
            //{
            //    Console.WriteLine(person.Name);
            //}

        }
    }
}
