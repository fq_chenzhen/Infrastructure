﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using OpenCvSharp;

namespace Ai.SurveillanceWorker
{
    public delegate int DumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);

    public delegate int SemanticJudgmentDLL(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);

    public class DumptrashmonitorFactory
    {
        private static ConcurrentDictionary<int, DumptrashmonitorDLL> DumptrashmonitorDlls = new ConcurrentDictionary<int, DumptrashmonitorDLL>();

        public static DumptrashmonitorDLL Create(int index)
        {
            return DumptrashmonitorDlls.GetOrAdd(index,
                 k =>
                 {
                     var dumpTrashType = Type.GetType($"Ai.SurveillanceWorker.DumpTrash{index}");
                     var dumptrashmonitorDLL = dumpTrashType.GetMethod("dumptrashmonitorDLL");
                     return (DumptrashmonitorDLL)Delegate.CreateDelegate(typeof(DumptrashmonitorDLL), dumptrashmonitorDLL);
                 });
        }

        public static DumptrashmonitorDLL Build(int index)
        {
            return DumptrashmonitorDlls.GetOrAdd(index,
                k =>
                {
                    switch (index)
                    {
                        case 0:
                            return DumpTrash0.dumptrashmonitorDLL;
                        case 1:
                            return DumpTrash1.dumptrashmonitorDLL;
                        case 2:
                            return DumpTrash2.dumptrashmonitorDLL;
                        case 3:
                            return DumpTrash3.dumptrashmonitorDLL;
                        case 4:
                            return DumpTrash4.dumptrashmonitorDLL;
                        case 5:
                            return DumpTrash5.dumptrashmonitorDLL;
                        case 6:
                            return DumpTrash6.dumptrashmonitorDLL;
                        case 7:
                            return DumpTrash7.dumptrashmonitorDLL;
                        case 8:
                            return DumpTrash8.dumptrashmonitorDLL;
                        case 9:
                            return DumpTrash9.dumptrashmonitorDLL;
                        case 10:
                            return DumpTrash10.dumptrashmonitorDLL;
                        default:
                            return DumpTrash0.dumptrashmonitorDLL;
                    }
                });
        }
    }

    public class SemanticJudgmentFactory
    {
        private static ConcurrentDictionary<int, SemanticJudgmentDLL> _semanticJudgmentDLLs = new ConcurrentDictionary<int, SemanticJudgmentDLL>();

        private static readonly List<object> _lockObjects = new List<object>
        {
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
            new object(),
        };

        private static readonly object _lockObject = new object();

        public static int BuildAndInvoke(int index, int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr)
        {
            lock (_lockObjects[index])
            {
                SemanticJudgmentDLL semanticJudgmentDLL;

                if (_semanticJudgmentDLLs.ContainsKey(index))
                    semanticJudgmentDLL = _semanticJudgmentDLLs[index];
                else
                    semanticJudgmentDLL = _semanticJudgmentDLLs.GetOrAdd(index, k =>
                     {
                         switch (index)
                         {
                             case 0:
                                 return Semantic0.semantic_judgment;
                             case 1:
                                 return Semantic1.semantic_judgment;
                             case 2:
                                 return Semantic2.semantic_judgment;
                             case 3:
                                 return Semantic3.semantic_judgment;
                             case 4:
                                 return Semantic4.semantic_judgment;
                             case 5:
                                 return Semantic5.semantic_judgment;
                             case 6:
                                 return Semantic6.semantic_judgment;
                             case 7:
                                 return Semantic7.semantic_judgment;
                             case 8:
                                 return Semantic8.semantic_judgment;
                             case 9:
                                 return Semantic9.semantic_judgment;
                             case 10:
                                 return Semantic10.semantic_judgment;
                             default:
                                 return API.semantic_judgment;
                         }
                     });

                return semanticJudgmentDLL(total_num, width, height, imagePtrs, tracks, outputPtr);
            }
        }
    }


#if DEBUG
    public class API
    {
        // 图像拼接和抛物识别
        [DllImport(@"river_mosaic.dll")]
        public static extern int rivermosaicDLL(IntPtr[] srcImage, IntPtr[] masks, IntPtr dstimage, int width, int height, int n);

        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic0
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic1
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic2
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }
    public class Semantic3
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic4
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic5
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic6
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic7
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }
    public class Semantic8
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic9
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic10
    {
        [DllImport(@"image_semantic_judgment.dll")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }


    public class DumpTrash0
    {
        [DllImport(@"riverdumptrashmonitor0.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
        [DllImport(@"riverdumptrashmonitor0.dll")]
        public static extern int generateMask(int width, int height, IntPtr mask);
    }

    public class DumpTrash1
    {
        [DllImport(@"riverdumptrashmonitor1.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }

    public class DumpTrash2
    {
        [DllImport(@"riverdumptrashmonitor2.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash3
    {
        [DllImport(@"riverdumptrashmonitor3.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash4
    {
        [DllImport(@"riverdumptrashmonitor4.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash5
    {
        [DllImport(@"riverdumptrashmonitor5.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash6
    {
        [DllImport(@"riverdumptrashmonitor6.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash7
    {
        [DllImport(@"riverdumptrashmonitor7.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash8
    {
        [DllImport(@"riverdumptrashmonitor8.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash9
    {
        [DllImport(@"riverdumptrashmonitor9.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash10
    {
        [DllImport(@"riverdumptrashmonitor10.dll")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
#else
    public class API
    {
        // 图像拼接和抛物识别
        [DllImport(@"river_mosaic.so")]
        public static extern int rivermosaicDLL(IntPtr[] srcImage, IntPtr[] masks, IntPtr dstimage, int width, int height, int n);

        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic0
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic1
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic2
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }
    public class Semantic3
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic4
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic5
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic6
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic7
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }
    public class Semantic8
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic9
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }

    public class Semantic10
    {
        [DllImport(@"image_semantic_judgment.so")]
        public static extern int semantic_judgment(int total_num, int width, int height, IntPtr[] imagePtrs, SemanticJudgmentTrack[] tracks, IntPtr outputPtr);
    }


    public class DumpTrash0
    {
        [DllImport(@"riverdumptrashmonitor0.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
        [DllImport(@"riverdumptrashmonitor0.so")]
        public static extern int generateMask(int width, int height, IntPtr mask);
    }

    public class DumpTrash1
    {
        [DllImport(@"riverdumptrashmonitor1.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }

    public class DumpTrash2
    {
        [DllImport(@"riverdumptrashmonitor2.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash3
    {
        [DllImport(@"riverdumptrashmonitor3.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash4
    {
        [DllImport(@"riverdumptrashmonitor4.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash5
    {
        [DllImport(@"riverdumptrashmonitor5.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash6
    {
        [DllImport(@"riverdumptrashmonitor6.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash7
    {
        [DllImport(@"riverdumptrashmonitor7.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash8
    {
        [DllImport(@"riverdumptrashmonitor8.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash9
    {
        [DllImport(@"riverdumptrashmonitor9.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
    public class DumpTrash10
    {
        [DllImport(@"riverdumptrashmonitor10.so")]
        public static extern int dumptrashmonitorDLL(int width, int height, IntPtr image, IntPtr mask, IntPtr inputPara, IntPtr outputPara);
    }
#endif

    [StructLayout(LayoutKind.Sequential)]
    public struct riverdumptrashmonitorInputParameter
    {
        int zhen;
        public int reset;
        float second;
        //public riverdumptrashmonitorInputParameter()
        //{
        //    zhen = 0;
        //    reset = 0;
        //    second = 0.0f;
        //}
    }


    [StructLayout(LayoutKind.Sequential)]
    public struct riverdumptrashmonitorOutputParameter
    {
        public int reset;   //0-未移动  1-移动

        public int find_dump_trash;  //探测到有人抛物

        [MarshalAs(UnmanagedType.Struct)]
        public targeEvent events;

    }

    [StructLayout(LayoutKind.Sequential)]
    public struct targeEvent
    {
        public int eventtype; // 1是抛物，2是泼水

        public int last_idx; // 最后一个数据所在处

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public int[] x; // 最后8次的x值。

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public int[] y; // 最后8次的y值。

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public int[] n; // 最后8次的大小。

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public int[] break_off; //如果发现断帧的现象，打个记号

    }


    [StructLayout(LayoutKind.Sequential)]
    public class targeEvent1
    {
        public int eventtype;  //抛物1  泼水2
        public int startX;
        public int starty;
        public int endX;
        public int endY;
        public int size;
        public targeEvent1()
        {
            eventtype = 1;
            startX = 0;
            starty = 0;
            endX = 0;
            endY = 0;
            size = 0;
        }
    }

    struct SaveEvidenceParam
    {
        public List<Rectangle> rects;
        public ModuleContext moduleParams;
    }

    public struct MosaicResponse
    {
        public int result;  //1-成功  0-输入图像尺寸不对  2-没有找到河道
        public string message;
        public Mat mask;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SemanticJudgmentOutputParameter
    {
        public float confidence; // 轨迹可靠性

        public float traceablity;// 是否可以追踪到抛物的源头
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SemanticJudgmentTrack
    {
        public int x;
        public int y;
        public int n;
    }

}
