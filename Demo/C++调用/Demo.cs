﻿using System;

public class Demo
{
    public Demo()
	{

	}

    public void Invok2()
    {
        riverdumptrashmonitorInputParameter input = new riverdumptrashmonitorInputParameter();
        input.reset = reset;
        IntPtr inputPtr = Marshal.AllocHGlobal(Marshal.SizeOf(input));
        Marshal.StructureToPtr(input, inputPtr, false);

        //riverdumptrashmonitorOutputParameter output = new riverdumptrashmonitorOutputParameter();
        IntPtr outputPtr = Marshal.AllocHGlobal(Marshal.SizeOf(output));
        Marshal.StructureToPtr(output, outputPtr, false);

        int ret = DumptrashmonitorFactory.Build(ModuleContext.JobOption.Index)
            .Invoke(frame.Width, frame.Height, frame.DataStart, riverMask.DataStart, inputPtr, outputPtr);

        output = (riverdumptrashmonitorOutputParameter)Marshal.PtrToStructure(outputPtr,
            typeof(riverdumptrashmonitorOutputParameter));
        input = (riverdumptrashmonitorInputParameter)Marshal.PtrToStructure(inputPtr,
            typeof(riverdumptrashmonitorInputParameter));
    }

    public void Invok1()
    {
        SemanticJudgmentOutputParameter outputParameter = new SemanticJudgmentOutputParameter();
        IntPtr outputPtr = Marshal.AllocHGlobal(Marshal.SizeOf(outputParameter));
        Marshal.StructureToPtr(outputParameter, outputPtr, false);

        SemanticJudgmentTrack[] tracks = new SemanticJudgmentTrack[total_num];
        for (int i = 0; i < riverOutput.events.last_idx + 1; i++)
        {
            tracks[i] = new SemanticJudgmentTrack
            {
                x = riverOutput.events.x[i],
                y = riverOutput.events.y[i],
                n = riverOutput.events.n[i]
            };
        }

        IntPtr[] imagePtrs = new IntPtr[8]; //当前图片后8张images的ptr
        var currImgIdx = 7;

        for (int i = frameArrClone.Length - 1; i >= 0; i = i - 2)
        {
            imagePtrs[currImgIdx] = frameArrClone[i].frameMat.DataStart;
            currImgIdx--;
            if (currImgIdx < 0)
            {
                break;
            }
        }

        var ret = SemanticJudgmentFactory.BuildAndInvoke(ModuleContext.JobOption.Index, total_num, width, height, imagePtrs, tracks, outputPtr);

        outputParameter = (SemanticJudgmentOutputParameter)Marshal.PtrToStructure(outputPtr,
            typeof(SemanticJudgmentOutputParameter));

        Marshal.FreeHGlobal(outputPtr);
    }

}
