﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;

namespace Ai.SurveillanceWorker
{
    public class ModuleManger : IModuleManger
    {

        private ConcurrentDictionary<string, AbstractModuleStrategy> _modules = new ConcurrentDictionary<string, AbstractModuleStrategy>();

        public AbstractModuleStrategy GetModuleStrategy(string strategy)
        {
            //return _modules.GetOrAdd(strategy, k =>
            //{
            //    var moduleStr = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(strategy.ToLower());

            //    //System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Namespace
            //    //var str =  nameof(ModuleLitterStrategy);
            //    var moduleType = Type.GetType($"Ai.SurveillanceWorker.Module{moduleStr}Strategy");

            //    return (AbstractModuleStrategy)Activator.CreateInstance(moduleType);
            //});

            strategy = strategy.ToLower();

            return _modules.GetOrAdd(strategy, k =>
            {
                switch (k)
                {
                    case "litter":
                        return new ModuleLitterStrategy();
                    default:
                        return new ModuleNullStrategy();
                }
                //return (AbstractModuleStrategy)Activator.CreateInstance(moduleType);
            });

        }
    }
}
