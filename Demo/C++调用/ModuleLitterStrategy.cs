﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Ai.SurveillanceWorker.Infrastructure;
using Ai.SurveillanceWorker.Repository;
using AnimatedGif;
using Microsoft.Extensions.DependencyInjection;

namespace Ai.SurveillanceWorker
{
    public class ModuleLitterStrategy : AbstractModuleStrategy
    {
        private Mat _litterRiverMask;  //抛物-河道mask
        private float _litterScale = 1; //抛物-真实视频帧与抛物输入视频帧的尺寸比值
        private bool _litterReset = true; //抛物-服务刚刚启动或者摄像头转动（变焦）结束
        private bool _maskLitterReset = true; //抛物-服务刚刚启动或者摄像头转动（变焦）结束

        //private static readonly object _lockSemanticObject = new object();

        private List<FieldInfo> _fieldInfos = new List<FieldInfo>();


        private TimeSpan _lastFrameTimeSpan = new TimeSpan(DateTime.UtcNow.Ticks);

        //public override async Task<int> DetectAsync(ModuleContext moduleContext)
        //{
        //    return await Task.Run(async () => await Detect(moduleContext));
        //}

        public override int Detect(ModuleContext moduleContext)
        {
            //1、解析传入的参数
            ModuleContext = moduleContext;
            FrameInfo thisFrame = ModuleContext.FrameArr[ModuleContext.FrameArr.Length - 1]; //注意这里都是浅拷贝，修改内部mat会影响到源

#if TRACE
            DateTime now = DateTime.Now;
            Stopwatch sw = new Stopwatch();
            sw.Reset();
            sw.Start();
            //ModuleContext.Logger.LogInformation($"-----------------------------------{thisFrame.index}");
#endif
            //moduleContext.Logger.LogError($"---No Detect {thisFrame.index}");
            //return 0;


            //1.1、判断是否已生成河道mask，如果生成不成功则弹出提示，不进行后续处理
            if (_litterRiverMask == null && !_litterReset)
            {
                ModuleContext.Logger.LogWarning(string.Format("未生成河道mask"));
                return 0;
            }

            float scale = (float)thisFrame.frameMat.Height / (float)ModuleContext.Config.FrameHeight;
            int newW = (int)(thisFrame.frameMat.Width / scale);
            int newH = (int)(thisFrame.frameMat.Height / scale);

            List<Rectangle> rects = new List<Rectangle>();
            Mat thisFrameResized = thisFrame.frameMat; //.Clone();

            riverdumptrashmonitorOutputParameter riverdumpOutput = new riverdumptrashmonitorOutputParameter();

            try
            {
                //2、重新调整当前视频帧的尺寸
                if (newH != ModuleContext.JobOption.FrameHeight)
                {
                    Cv2.Resize(thisFrameResized, thisFrameResized, new OpenCvSharp.Size(newW, newH));
                }

                //3、判断是否需要重新生成mask
                if (_maskLitterReset)
                {
                    //4、获取当前摄像头的预置mask并处理成识别算法需要的格式
                    MosaicResponse mosaicResult = mosaicRiverMask(thisFrameResized, ModuleContext.JobOption.Id, newW, newH);
                    ModuleContext.Logger.LogWarning(mosaicResult.message);
                    //5、如果mask绘制成功则给_riverMask赋值，否则将其值设为null
                    if (mosaicResult.result == 1)
                    {
                        _litterRiverMask = mosaicResult.mask;
                        DumpTrash0.generateMask(newW, newH, _litterRiverMask.DataStart);
                        _maskLitterReset = false;
                    }
                    else
                    {
                        _litterRiverMask = null;
                    }
                }

                if (_litterRiverMask == null) return 0;

                //6、调用抛物识别算法
                if (this._lastFrameTimeSpan.Subtract(new TimeSpan(DateTime.UtcNow.Ticks)).Seconds > 15) //超过15秒无视频帧把模型清空下
                    _litterReset = true;

                rects = callDll(thisFrameResized, _litterRiverMask, ref _litterReset, ref riverdumpOutput);

                this._lastFrameTimeSpan = new TimeSpan(DateTime.UtcNow.Ticks);

                //9、处理完之后将_reset参数设为false
                _litterReset = false;
            }
            finally
            {
                //thisFrameResized.Dispose();
                thisFrameResized = null;
            }

            //7、识别结果后处理
            if (rects.Count == 0) return 0;

            //if (thisFrame.index % 200 != 0) return 0;

            // 队列没满不取证
            if (ModuleContext.FrameArr.Length < ModuleContext.Config.BatchSize) return 0;


#if TRACE
            sw.Stop();
            ModuleContext.Logger.LogWarning($"riverdumptrashmonitor...耗时{sw.ElapsedMilliseconds}ms");
#endif

            //_litterReset = true;

            // 异步存储视频，不然线程会出错
            var frameArrClone = (from m in ModuleContext.FrameArr select m.Clone()).ToList();

            ModuleContext.FrameArr = null;

            string saveDir = Path.Combine(ModuleContext.Config.EvidenceDir, DateTime.Now.ToString("yyyy/MM/dd"), ModuleContext.Config.CamId.ToString());
            if (Directory.Exists(saveDir) == false) Directory.CreateDirectory(saveDir);


            var relativePathBase = $"{ModuleContext.Config.RelativeDir.TrimEnd('/')}/{DateTime.Now.ToString("yyyy/MM/dd")}/{ModuleContext.Config.CamId}";
            var eFileName = $"{thisFrame.time.ToString("yyyyMMddHHmmss")}";

            //8、存储取证视频
            string savePathVideo = Path.Combine(saveDir, $"{eFileName}.avi");
            string relativePathVideo = $"{relativePathBase}/{eFileName}.avi";

            var state = new
            {
                riverdumpOutput,
                frameArrClone,
                relativePathBase,
                saveDir,
                eFileName,
                relativePathVideo,
                savePathVideo,
                newW,
                newH,
                rects,
                thisFrameDateTime = thisFrame.time
            };

            ThreadPool.QueueUserWorkItem(new WaitCallback(judegmentAndPublishData), state);

            thisFrame = null;

            return 1;
        }

        private void judegmentAndPublishData(object obj)
        {

            var context = obj as dynamic;

            //List<string> imageFilePaths = Enumerable.ToList(context.imageFilePaths);
            FrameInfo[] frameArrClone = Enumerable.ToArray<FrameInfo>(context.frameArrClone);
            riverdumptrashmonitorOutputParameter riverdumpOutput = context.riverdumpOutput;
            string relativePathBase = context.relativePathBase;
            string saveDir = context.saveDir;
            string eFileName = context.eFileName;
            string relativePathVideo = context.relativePathVideo;
            string savePathVideo = context.savePathVideo;
            int newW = context.newW;
            int newH = context.newH;
            List<Rectangle> rects = context.rects;
            DateTime thisFrameDateTime = context.thisFrameDateTime;

            try
            {
                SemanticJudgmentOutputParameter semanticOutput;
                //lock (_lockSemanticObject)
                //{
                // 处理模块筛选误识别，进一步判断是否有抛物
#if TRACE
                DateTime now = DateTime.Now;
                Stopwatch sw = new Stopwatch();
                sw.Reset();
                sw.Start();
#endif
                semanticOutput = callSemanticJudgment(frameArrClone, riverdumpOutput, newW, newH);

#if TRACE
                sw.Stop();
                ModuleContext.Logger.LogWarning($"image_semantic_judgment...耗时{sw.ElapsedMilliseconds}ms");
#endif
                //}

                var otherOptions = ModuleContext.Config.OtherOptions.Split(',');
                var confidenceThreshold = otherOptions.Where(s =>
                        string.Compare(s.Split(':')[0], "confidence", CultureInfo.CurrentCulture,
                            CompareOptions.IgnoreCase) == 0)
                    .Select(s => s.Split(':')[1]).FirstOrDefault();
                var traceablityThreshold = otherOptions.Where(s =>
                        string.Compare(s.Split(':')[0], "traceablity", CultureInfo.CurrentCulture,
                            CompareOptions.IgnoreCase) == 0)
                    .Select(s => s.Split(':')[1]).FirstOrDefault();

                var clearAndReturn = false;
                if (semanticOutput.confidence < Convert.ToDouble(confidenceThreshold)) clearAndReturn = true;
                if (semanticOutput.traceablity < Convert.ToDouble(traceablityThreshold)) clearAndReturn = true;
                if (clearAndReturn)
                {
                    if (frameArrClone == null) return;
                    for (int i = 0; i < frameArrClone.Length; i++)
                    {
                        frameArrClone[i].Dispose();
                    }

                    return;
                }

                var imageFileNames = this.saveEvidenceVideo(frameArrClone, saveDir, eFileName, savePathVideo,
                    ModuleContext.JobOption.Fps, newW, newH, rects);

                var imageFilePaths = imageFileNames.Select(s => $"{relativePathBase}/{s}").ToList();

                var respAiRepository =
                    ModuleContext.JobOption.ServiceProvider.GetService<IAiRepository<LitterDetect>>();
                respAiRepository.Insert(new LitterDetect()
                {
                    CamId = ModuleContext.Config.CamId,
                    Time = thisFrameDateTime,
                    ImageFilePaths = imageFilePaths,
                    Video = relativePathVideo,
                    Rectangles = rects
                });
            }
            catch (Exception ex)
            {
                ModuleContext.Logger.LogWarning($"---judegmentAndPublishData:{ex.Message}");
            }
            finally
            {
                if (frameArrClone != null)
                {
                    for (int i = 0; i < frameArrClone.Length; i++)
                    {
                        frameArrClone[i].Dispose();
                    }
                }

                frameArrClone = null;

                //GC.Collect();
            }
        }

        [Obsolete]
        private void afterProcess(object obj)
        {
            //8、存储取证视频
            //string savePathVideo = Path.Combine(saveDir, $"{eFileName}.avi");
            //string relativePathVideo = Path.Combine(relativePathBase, $"{eFileName}.avi");
#if TRACE
            int maxWorkThreads = 0, maxComplitionThreads = 0, avWorkThreads = 0, avComplitionThreads = 0;
            ThreadPool.GetMaxThreads(out maxWorkThreads, out maxComplitionThreads);
            ThreadPool.GetAvailableThreads(out avWorkThreads, out avComplitionThreads);
            Console.WriteLine($"==================================== maxWorkThreads: {maxWorkThreads}, maxComplitionThreads: {maxComplitionThreads}, avWorkThreads: {avWorkThreads}, avComplitionThreads: {avComplitionThreads} ====================================");
            Console.WriteLine($"==================================== WorkThreads: {maxWorkThreads - avWorkThreads}, GetCurrentProcessorId:{Thread.GetCurrentProcessorId()} ====================================");
#endif

            var context = obj as dynamic;

            string saveDir = context.saveDir;
            string eFileName = context.eFileName;
            string relativePathBase = context.relativePathBase;
            string saveSourcePathVideo = context.saveSourcePathVideo;
            string savePathVideo = context.savePathVideo;
            string relativePathVideo = context.relativePathVideo;


            int newW = context.newW;
            int newH = context.newH;

            //List<FrameInfo> frameArrClone = context.frameArrClone;
            List<Rectangle> rects = context.rects;
            DateTime thisFrameDateTime = context.thisFrameDateTime;


            //9、存储gif
            string savePathGif = Path.Combine(saveDir, $"{eFileName}.gif");
            string relativePathGif = Path.Combine(relativePathBase, $"{eFileName}.gif");

            //10、存储缩略图
            string savePathThumb = Path.Combine(saveDir, $"{eFileName}.jpg");
            string relativePathThumb = Path.Combine(relativePathBase, $"{eFileName}.jpg");

            var mygif = AnimatedGif.AnimatedGif.Create(savePathGif, 200);
            var capture = new VideoCapture(relativePathVideo);
            //using (VideoWriter writer = new VideoWriter(savePathVideo, VideoWriter.FourCC('M', 'J', 'P', 'G'),
            //    ModuleContext.JobOption.Fps, new OpenCvSharp.Size(newW, newH), true))

            try
            {
                var i = 0;
                var totalLength = capture.Get(CaptureProperty.FrameCount);
                while (i < totalLength)
                {
                    Mat frameResized = new Mat();
                    IntPtr ptr = frameResized.Ptr();

                    try
                    {
                        var success = capture.Read(frameResized);

                        if (!success || frameResized.Empty() || frameResized == null)
                        {
                            //if (src != null && !src.IsDisposed) src.Dispose();
                            //src.Dispose();
                            //src = null;
                            frameResized.Dispose();
                            frameResized = null;
                            i++;
                            continue;
                        }

                        //8、存储取证视频
                        //drawRect(frameResized, rects);
                        //writer.Write(frameResized);

                        //10、存储缩略图
                        if (i == 10)
                        {
                            Cv2.Resize(frameResized, frameResized, new OpenCvSharp.Size(300, newH * 300 / newW));
                            frameResized.SaveImage(savePathThumb);
                        }

                        //9、存储gif
                        if (i <= totalLength - ModuleContext.JobOption.Fps * 3)
                        {
                            i++;
                            continue;
                        }

                        if (i % ((int)(ModuleContext.JobOption.Fps / 4)) == 0)
                        {
                            Cv2.Resize(frameResized, frameResized, new OpenCvSharp.Size(800, newH * 800 / newW));
                            using (Bitmap btm = BitmapConverter.ToBitmap(frameResized))
                            {
                                mygif.AddFrame(btm, delay: -1);
                            }
                        }

                        frameResized.Dispose();
                        frameResized = null;

                        i++;
                    }
                    catch (Exception ex)
                    {
                        frameResized.Dispose();
                        frameResized = null;
                    }
                }
            }
            finally
            {
                mygif.Dispose();
                capture.Dispose();
                mygif = null;
                capture = null;
            }

            //12、数据入库
            var respAiRepository = ModuleContext.JobOption.ServiceProvider.GetService<IAiRepository<LitterDetect>>();
            respAiRepository.Insert(new LitterDetect()
            {
                CamId = ModuleContext.Config.CamId,
                Time = thisFrameDateTime,
                //Evidence = relativePathGif,
                Video = relativePathVideo,
                //Thumb = relativePathThumb,
                Rectangles = rects
            });

            rects.Clear();
            rects = null;
            respAiRepository = null;
            context = null;
            obj = null;
            //File.Delete(saveSourcePathVideo);

            //GC.Collect();
        }

        /// <summary>
        /// 存储取证原始视频
        /// </summary>
        /// <param name="obj"></param>
        public void saveSourceVideo(string savePath, int fps, int newW, int newH)
        {
            using (VideoWriter writer = new VideoWriter(savePath, VideoWriter.FourCC('M', 'J', 'P', 'G'), fps, new OpenCvSharp.Size(newW, newH), true))
            {
                for (int i = 0; i < ModuleContext.FrameArr.Length; i++)
                {
                    writer.Write(ModuleContext.FrameArr[i].frameMat);
                }
            }
        }

        /// <summary>
        /// 取证视频
        /// </summary>
        /// <param name="savePath"></param>
        /// <param name="fps"></param>
        private List<string> saveEvidenceVideo(FrameInfo[] frameArr, string saveDir, string eFileName, string savePath, int fps, int newW, int newH, List<Rectangle> rects)
        {
            List<string> imageFileNames = new List<string>();

            VideoWriter writer = new VideoWriter(savePath, VideoWriter.FourCC('M', 'J', 'P', 'G'), fps, new OpenCvSharp.Size(newW, newH), true);
            try
            {

                for (int i = 0; i < frameArr.Length; i++)
                {
                    if (frameArr[i].frameMat == null || frameArr[i].frameMat.IsDisposed)
                    {
                        ModuleContext.Logger.LogWarning($"frameMat IsDisposed:{i}");
                        continue;
                    }

                    //writer.Write(frameArr[i].frameMat);

                    //using (frameArr[i])
                    var mat = frameArr[i].frameMat;
                    try
                    {
                        drawRect(mat, rects);
                        writer.Write(mat);

                        //10、存储缩略图
                        if (i > (frameArr.Length - fps - 3) && i % 5 == 0)
                        {
                            var imageFileName = $"{eFileName}_{frameArr[i].index}.jpg";
                            imageFileNames.Add(imageFileName);

                            Cv2.Resize(mat, mat, new OpenCvSharp.Size(800, newH * 800 / newW));
                            mat.SaveImage(Path.Combine(saveDir, imageFileName));
                        }
                    }
                    finally
                    {
                        mat.Dispose();
                        mat = null;
                    }
                }
            }
            finally
            {
                writer.Dispose();
                writer = null;
                frameArr = null;
            }

            return imageFileNames;
        }

        //private Task saveEvidenceVideoAsync(FrameInfo[] frameArr, string savePath, int fps, List<Rectangle> rects)
        //{
        //    return Task.Run(() => { this.saveEvidenceVideo(frameArr, savePath, fps, rects); });
        //}

        /// <summary>
        /// 存储取证视频
        /// </summary>
        /// <param name="obj"></param>
        public void saveVideo(string savePath, int fps, int newW, int newH, int batchSize, List<Rectangle> rects)
        {
            using (VideoWriter writer = new VideoWriter(savePath, VideoWriter.FourCC('M', 'J', 'P', 'G'), fps, new OpenCvSharp.Size(newW, newH), true))
            {
                for (int i = ModuleContext.FrameArr.Length - batchSize; i < ModuleContext.FrameArr.Length; i++)
                {
                    using (Mat frameResized = new Mat())
                    {
                        Cv2.Resize(ModuleContext.FrameArr[i].frameMat, frameResized, new OpenCvSharp.Size(newW, newH));
                        drawRect(frameResized, rects);
                        writer.Write(frameResized);
                    }
                }
            }
        }

        // 存储gif
        public void saveGif(string savePath, int fps, int newW, int newH, List<Rectangle> rects)
        {
            int width = 800;
            float scale = (float)((float)newW / (float)800);
            int height = (int)((float)newH / (float)scale);
            Mat frameResized = new Mat(width, height, ModuleContext.FrameArr[0].frameMat.Type());
            using (var mygif = new AnimatedGifCreator(savePath, 250))
            {
                for (int i = 0; i < ModuleContext.FrameArr.Length; i++)
                {
                    if (i <= ModuleContext.FrameArr.Length - fps * 3)
                    {
                        continue;
                    }

                    if (i % ((int)(fps / 4)) == 0)
                    {
                        Cv2.Resize(ModuleContext.FrameArr[i].frameMat, frameResized, new OpenCvSharp.Size(newW, newH));
                        drawRect(frameResized, rects);
                        Cv2.Resize(frameResized, frameResized, new OpenCvSharp.Size(width, height));
                        Bitmap a = BitmapConverter.ToBitmap(frameResized);
                        mygif.AddFrame(a, delay: -1, quality: GifQuality.Bit8);
                        a.Dispose();
                    }
                }
            }
            frameResized.Dispose();
        }

        // 存储缩略图
        public void saveThumb(string savePath, int fps, int newW, int newH, List<Rectangle> rects)
        {
            int index = 10;   //取事件发生时前x帧的图像作为缩略图
            float scale = (float)newW / (float)300;
            int width = 300;
            int height = (int)((float)newH / (float)scale);
            using (Mat frameResized = new Mat())
            {
                for (int i = 0; i < ModuleContext.FrameArr.Length; i++)
                {
                    if (i == ModuleContext.FrameArr.Length - index)
                    {
                        Cv2.Resize(ModuleContext.FrameArr[i].frameMat, frameResized, new OpenCvSharp.Size(newW, newH));
                        drawRect(frameResized, rects);
                        Cv2.Resize(frameResized, frameResized, new OpenCvSharp.Size(width, height));
                        frameResized.SaveImage(savePath);
                    }
                }
            }
        }


        public static bool inArea(Rect person, Rect area)
        {
            if (person.Left >= area.Left && person.Left <= area.Right && person.Top >= area.Top && person.Top <= area.Bottom)
            {
                return true;
            }
            if (person.Left >= area.Left && person.Left <= area.Right && person.Bottom >= area.Top && person.Bottom <= area.Bottom)
            {
                return true;
            }
            if (person.Right >= area.Left && person.Right <= area.Right && person.Top >= area.Top && person.Top <= area.Bottom)
            {
                return true;
            }
            if (person.Right >= area.Left && person.Right <= area.Right && person.Bottom >= area.Top && person.Bottom <= area.Bottom)
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// 绘制矩形框
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="rects"></param>
        /// <returns></returns>
        public static void drawRect(Mat frame, List<Rectangle> rects)
        {
            List<List<OpenCvSharp.Point>> points = new List<List<OpenCvSharp.Point>>();
            for (int i = 0; i < rects.Count; i++)
            {
                List<OpenCvSharp.Point> tempPoints = new List<OpenCvSharp.Point>();
                tempPoints.Add(new OpenCvSharp.Point(rects[i].Left, rects[i].Top));
                tempPoints.Add(new OpenCvSharp.Point(rects[i].Left, rects[i].Top + rects[i].Height));
                tempPoints.Add(new OpenCvSharp.Point(rects[i].Left + rects[i].Width, rects[i].Top + rects[i].Height));
                tempPoints.Add(new OpenCvSharp.Point(rects[i].Left + rects[i].Width, rects[i].Top));
                points.Add(tempPoints);
            }
            Cv2.DrawContours(frame, points, -1, Scalar.LightGreen, 2);
        }

        /// <summary>
        /// 识别后处理
        /// </summary>
        /// <returns></returns>
        private SemanticJudgmentOutputParameter callSemanticJudgment(FrameInfo[] frameArrClone, riverdumptrashmonitorOutputParameter riverOutput, int width, int height)
        {
            int total_num = riverOutput.events.last_idx + 1;

            SemanticJudgmentOutputParameter outputParameter = new SemanticJudgmentOutputParameter();
            IntPtr outputPtr = Marshal.AllocHGlobal(Marshal.SizeOf(outputParameter));
            Marshal.StructureToPtr(outputParameter, outputPtr, false);

            SemanticJudgmentTrack[] tracks = new SemanticJudgmentTrack[total_num];
            for (int i = 0; i < riverOutput.events.last_idx + 1; i++)
            {
                tracks[i] = new SemanticJudgmentTrack
                {
                    x = riverOutput.events.x[i],
                    y = riverOutput.events.y[i],
                    n = riverOutput.events.n[i]
                };
            }
            //IntPtr trackPtr = Marshal.UnsafeAddrOfPinnedArrayElement(tracks, 0);

            IntPtr[] imagePtrs = new IntPtr[8]; //当前图片后8张images的ptr
            var currImgIdx = 7;

            for (int i = frameArrClone.Length - 1; i >= 0; i = i - 2)
            {
                imagePtrs[currImgIdx] = frameArrClone[i].frameMat.DataStart;
                currImgIdx--;
                if (currImgIdx < 0)
                {
                    break;
                }
            }

            try
            {
                //if (frameArrClone[0].index > 200)
                //    return new SemanticJudgmentOutputParameter() { confidence = 1, traceablity = 1 };
                //else
                //    return new SemanticJudgmentOutputParameter();

                //var ret = API.semantic_judgment(total_num, width, height, imagePtrs, tracks, outputPtr);

                var ret = SemanticJudgmentFactory.BuildAndInvoke(ModuleContext.JobOption.Index, total_num, width, height, imagePtrs, tracks, outputPtr);

                ModuleContext.Logger.LogWarning($"------------------------------ semantic_judgment return: {ret} ----------------------");

                if (ret == 0) return outputParameter;

                outputParameter = (SemanticJudgmentOutputParameter)Marshal.PtrToStructure(outputPtr,
                    typeof(SemanticJudgmentOutputParameter));
            }
            catch (Exception ex)
            {
                ModuleContext.Logger.LogWarning($"---semanticJudgment:{ex.Message}");
            }
            finally
            {
                frameArrClone = null;
                Marshal.FreeHGlobal(outputPtr);
            }

            return outputParameter;
        }

        /// <summary>
        /// 调用识别算法
        /// </summary>
        /// <param name="frame">当前帧</param>
        /// <param name="riverMask">当前河道mask</param>
        /// <param name="firstFrame">是否需要重置算法</param>
        /// <returns></returns>
        private List<Rectangle> callDll(Mat frame, Mat riverMask, ref bool needReset, ref riverdumptrashmonitorOutputParameter output)
        {
            //1 准备输入输出结构体
            int reset = needReset ? 1 : 0;
            // test   每1万帧reset一次
            //if (frameIndex % 10000 == 0)
            //{
            //    reset = 1;
            //}
            riverdumptrashmonitorInputParameter input = new riverdumptrashmonitorInputParameter();
            input.reset = reset;
            IntPtr inputPtr = Marshal.AllocHGlobal(Marshal.SizeOf(input));
            Marshal.StructureToPtr(input, inputPtr, false);

            //riverdumptrashmonitorOutputParameter output = new riverdumptrashmonitorOutputParameter();
            IntPtr outputPtr = Marshal.AllocHGlobal(Marshal.SizeOf(output));
            Marshal.StructureToPtr(output, outputPtr, false);

            //2 调用识别算法  
            //            int ret = DumpTrash0.dumptrashmonitorDLL(frame.Width, frame.Height, frame.DataStart, riverMask.DataStart, inputPtr, outputPtr);

            // 反射调用
            //            var dumptrashmonitorDLL = typeof(DumpTrash0).GetMethod("dumptrashmonitorDLL");
            //            int ret = (int)dumptrashmonitorDLL.Invoke(null, new object[] { frame.Width, frame.Height, frame.DataStart, riverMask.DataStart, inputPtr, outputPtr });

            try
            {
                int ret = DumptrashmonitorFactory.Build(ModuleContext.JobOption.Index)
                    .Invoke(frame.Width, frame.Height, frame.DataStart, riverMask.DataStart, inputPtr, outputPtr);

                output = (riverdumptrashmonitorOutputParameter)Marshal.PtrToStructure(outputPtr,
                    typeof(riverdumptrashmonitorOutputParameter));
                input = (riverdumptrashmonitorInputParameter)Marshal.PtrToStructure(inputPtr,
                    typeof(riverdumptrashmonitorInputParameter));

                //ModuleContext.Logger.LogWarning($"------------ callDll ret:{ret} input:{input.ToJson()} output:{output.ToJson()}");

            }
            catch (Exception ex)
            {
                ModuleContext.Logger.LogWarning($"callDll:{ex.Message}");
            }
            finally
            {
                Marshal.FreeHGlobal(inputPtr);
                Marshal.FreeHGlobal(outputPtr);
            }

            //test 每10秒钟模拟一次抛物行为
            //if (frameIndex % 200 == 0 && frameIndex<100000)
            //{
            //    GC.Collect();
            //    output.find_dump_trash = 1;
            //    output.events1.size = 100;
            //    output.events1.startX = 1;
            //    output.events1.starty = 1;
            //    output.events1.endX = 100;
            //    output.events1.endY = 100;
            //}
            //else
            //{
            //    output.find_dump_trash = 0;
            //}
            //3 输出结果后处理
            //needReset = output.reset == 1 ? true : false;

            List<Rectangle> rects = new List<Rectangle>();
            if (output.reset == 1)
            {
                //摄像头经过移动，已趋于稳定
                needReset = true;
            }

            if (output.find_dump_trash != 0)
            {
                var lastIndex = output.events.last_idx;
                var o_x = output.events.x[0] < output.events.x[lastIndex] ? output.events.x[0] : output.events.x[lastIndex];
                var o_y = output.events.y[0] < output.events.y[lastIndex] ? output.events.y[0] : output.events.y[lastIndex];

                var o_width = Math.Abs(output.events.x[lastIndex] - output.events.x[0]);
                var o_height = Math.Abs(output.events.y[lastIndex] - output.events.y[0]);

                var center_x = o_x + o_width / 2;
                var center_y = o_y + o_height / 2;

                var max_length = o_width > o_height ? o_width : o_height;
                max_length = Convert.ToInt32(max_length * 1.5);

                var x = center_x - max_length / 2;
                var y = center_y - max_length / 2;

                rects.Add(new Rectangle(x, y, max_length, max_length));

                //GC.Collect();

                //foreach (FieldInfo field in this.getOutputFieldInfos())
                //{
                //    targeEvent value = (targeEvent)field.GetValue(output);
                //    if (value.size > 0)
                //    {
                //        rects.Add(getRect(output.events1));
                //    }
                //}
            }

            return rects;
        }

        private List<FieldInfo> getOutputFieldInfos()
        {
            if (_fieldInfos.Count > 0) return _fieldInfos;

            foreach (FieldInfo field in typeof(riverdumptrashmonitorOutputParameter).GetFields(BindingFlags.Instance |
                                                                                               BindingFlags.NonPublic |
                                                                                               BindingFlags.Public))
            {
                if (field.Name.IndexOf("event") >= 0)
                {
                    _fieldInfos.Add(field);
                }
            }

            return _fieldInfos;
        }

        /// <summary>
        /// 图像拼接生成河道mask
        /// </summary>
        public MosaicResponse mosaicRiverMask(Mat thisFrameResized, string jobKey, int newW, int newH)
        {
            string message = "mask"; //返回消息
            int ret;
            string sourceImagePath = Path.Combine(Utils.GetApplicationPath(), "river_edges", jobKey, "image_{0}.jpg");
            string sourceMaskPath = Path.Combine(Utils.GetApplicationPath(), "river_edges", jobKey, "mask_{0}.jpg");
            Mat[] imageMatArr = new Mat[5]; //原始图片
            Mat[] maskMatArr = new Mat[5]; //mask图片
            IntPtr[] ptrImageArr = new IntPtr[5]; //原始图片的ptr
            IntPtr[] ptrMaskArr = new IntPtr[5]; //mask的ptr
            int maskCount = 0;
            MosaicResponse response = new MosaicResponse();

            try
            {
                for (int i = 0; i < 5; i++)
                {
                    string imagePath = string.Format(sourceImagePath, i.ToString());
                    string maskPath = string.Format(sourceMaskPath, i.ToString());
                    if (File.Exists(imagePath) && File.Exists(maskPath))
                    {
                        imageMatArr[i] = new Mat(imagePath, ImreadModes.Color);
                        maskMatArr[i] = new Mat(maskPath, ImreadModes.Grayscale);

                        Cv2.Resize(imageMatArr[i], imageMatArr[i], new OpenCvSharp.Size(newW, newH));
                        Cv2.Resize(maskMatArr[i], maskMatArr[i], new OpenCvSharp.Size(newW, newH));
                        double temp = Cv2.Threshold(maskMatArr[i], maskMatArr[i], 25, 255, ThresholdTypes.Binary);

                        ptrImageArr[i] = imageMatArr[i].DataStart;
                        ptrMaskArr[i] = maskMatArr[i].DataStart;
                        maskCount++;
                    }
                    else
                    {
                        imageMatArr[i] = null;
                        maskMatArr[i] = null;
                    }
                }

                if (maskCount == 0)
                {
                    message = string.Format("河道mask生成出错，路径{0}中不存在预置的河道mask",
                        Path.Combine(Utils.GetApplicationPath(), "river_edges", jobKey));
                    ret = 0;
                }
                else
                {
                    //thisFrameResized.SaveImage(@"C:\Users\coolman\Desktop\temp\cur.jpg");
                    //imageMatArr[0].SaveImage(@"C:\Users\coolman\Desktop\temp\source.jpg");
                    //maskMatArr[0].SaveImage(@"C:\Users\coolman\Desktop\temp\mask.jpg");

                    //ret = API.rivermosaicDLL(ptrImageArr, ptrMaskArr, thisFrameResized.DataStart, newW, newH,
                    //    maskCount);
                    //switch (ret)
                    //{
                    //    case 0:
                    //        message = "河道mask生成出错：输入图像尺寸大小不一";
                    //        break;
                    //    case 1:
                    //        message = "河道mask生成成功！";
                    //        break;
                    //    case 2:
                    //        message = "河道mask生成出错：当前摄像头画面内无河道";
                    //        break;
                    //    default:
                    //        break;
                    //}
                }

                response.result = 1; //ret;
                response.message = message;
                response.mask = maskMatArr[0];
            }
            catch (Exception ex)
            {
                ModuleContext.Logger.LogWarning($"---mosaicRiverMask:${ex.Message}");
            }
            finally
            {
                for (int i = 0; i < 5; i++)
                {
                    if (imageMatArr[i] != null)
                    {
                        imageMatArr[i].Dispose();
                        imageMatArr[i] = null;
                    }
                    if (i == 0) continue;
                    if (maskMatArr[i] != null)
                    {
                        maskMatArr[i].Dispose();
                        maskMatArr[i] = null;
                    }
                }
                imageMatArr = null;
                maskMatArr = null;
                ptrImageArr = null;
                ptrMaskArr = null;
            }

            return response;
        }

        /// <summary>
        /// 形成矩形框
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        //public static Rectangle getRect(targeEvent target)
        //{
        //    bool left2right = target.endX > target.startX ? true : false;
        //    int oldWidth = Math.Abs(target.endX - target.startX);
        //    int oldHeight = Math.Abs(target.endY - target.starty);
        //    int oldLeft = left2right ? target.startX : target.endX;
        //    int oldTop = target.starty;
        //    int centerX = oldLeft + oldWidth / 2;
        //    int centerY = oldTop + oldHeight / 2;
        //    int newSize = oldWidth > oldHeight ? oldWidth + 100 : oldHeight + 100;
        //    int newLeft = centerX - newSize / 2;
        //    int newTop = centerY - newSize / 2;
        //    return new Rectangle(newLeft, newTop, newSize, newSize);
        //}
    }
}
