﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Topshelf;

namespace TopSelfDemo
{
    public class TownCrier : ServiceControl
    {
        List<Process> pList = new List<Process>();

        //private Timer _timer = null;
        static readonly ILog _log = LogManager.GetLogger(typeof(TownCrier));
        public TownCrier()
        {
            //_timer = new Timer(1000) { AutoReset = true };
            //_timer.Elapsed += (sender, eventArgs) => _log.Info(DateTime.Now);
        }
        public bool Start(HostControl hostControl)
        {
            _log.Info(ConfigurationManager.AppSettings["serviceDescription"] + "开始...");
            var batList = Directory.GetFiles("./", ConfigurationManager.AppSettings["cmd"]).ToList();

            batList.ForEach(s =>
            {
                Task.Factory.StartNew(() =>
                {
                    Process p = new Process();
                    try
                    {
                        p.StartInfo.FileName = s;
                        p.StartInfo.Arguments = "";
                        //p.StartInfo.RedirectStandardInput = true;
                        //p.StartInfo.RedirectStandardOutput = true;
                        //p.StartInfo.RedirectStandardError = true;
                        p.StartInfo.UseShellExecute = false;
                        p.StartInfo.CreateNoWindow = true;
                        //p.OutputDataReceived += new DataReceivedEventHandler(outputDataReceived);
                        p.Start();
                        //p.BeginOutputReadLine();
                        p.WaitForExit();
                        pList.Add(p);
                    }
                    catch (Exception ex)
                    {
                        p.Close();
                        _log.Info(ex.Message);
                    }
                });
            });

            return true;

            #region old
            /*
            //_timer.Start();
            var cmdLists = File.ReadLines(ConfigurationManager.AppSettings["cmd"]);
            if (cmdLists.Where(s => s.ToLower().StartsWith("start")).Count() > 0)
            {
                cmdLists.ToList().ForEach(c =>
                {
                    var cmd = c.ToLower().Replace("start ", "");
                    if (string.IsNullOrEmpty(cmd)) return;

                    Task.Factory.StartNew(() =>
                    {
                        Process p = new Process();
                        try
                        {

                            p.StartInfo.FileName = "cmd.exe";
                            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
                            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
                            //p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
                            //p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
                            p.StartInfo.CreateNoWindow = true;//不显示程序窗口
                            p.Start();//启动程序

                            //向cmd窗口发送输入信息
                            p.StandardInput.WriteLine(cmd);

                            //p.StandardInput.AutoFlush = true;
                            //p.StandardInput.WriteLine("exit");
                            //向标准输入写入要执行的命令。这里使用&是批处理命令的符号，表示前面一个命令不管是否执行成功都执行后面(exit)命令，如果不执行exit命令，后面调用ReadToEnd()方法会假死
                            //同类的符号还有&&和||前者表示必须前一个命令执行成功才会执行后面的命令，后者表示必须前一个命令执行失败才会执行后面的命令



                            //获取cmd窗口的输出信息
                            //string output = p.StandardOutput.ReadToEnd();

                            //StreamReader reader = p.StandardOutput;
                            //string line=reader.ReadLine();
                            //while (!reader.EndOfStream)
                            //{
                            //    str += line + "  ";
                            //    line = reader.ReadLine();
                            //}

                            p.WaitForExit();//等待程序执行完退出进程
                            p.Close();

                            pList.Add(p);
                        }
                        catch (Exception ex)
                        {
                            p.Close();
                            _log.Info(ex.Message);
                        }
                    });
                });

            }
            else
            {
                var batList = Directory.GetFiles("./",ConfigurationManager.AppSettings["cmd"]).ToList();

                batList.ForEach(s => {
                    Task.Factory.StartNew(() =>
                    {
                        Process p = new Process();
                        try
                        {
                            p.StartInfo.FileName = s;
                            p.StartInfo.Arguments = "";
                            //p.StartInfo.RedirectStandardInput = true;
                            //p.StartInfo.RedirectStandardOutput = true;
                            //p.StartInfo.RedirectStandardError = true;
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.CreateNoWindow = true;
                            //p.OutputDataReceived += new DataReceivedEventHandler(outputDataReceived);
                            p.Start();
                            //p.BeginOutputReadLine();
                            p.WaitForExit();
                            pList.Add(p);
                        }
                        catch (Exception ex)
                        {
                            p.Close();
                            _log.Info(ex.Message);
                        }
                    });
                });
            }
            return true;
             */
            #endregion
            
        }

        public bool Stop(HostControl hostControl)
        {
            pList.ForEach(p => { p.Close(); });

            killProcess(ConfigurationManager.AppSettings["killProcessName"]);
            _log.Info(ConfigurationManager.AppSettings["serviceDescription"] + "结束...");
            return true;
            //throw new NotImplementedException();
        }

        private void outputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                _log.Info(e.Data);
            }
        }

        private void killProcess(string processName)
        {
            System.Diagnostics.Process myproc = new System.Diagnostics.Process();
            //得到所有打开的进程   
            try
            {
                foreach (Process thisproc in Process.GetProcessesByName(processName))
                {
                    //找到程序进程,kill之。
                    if (!thisproc.CloseMainWindow())
                    {
                        thisproc.Kill();
                    }
                }

            }
            catch (Exception Exc)
            {
                _log.Info(Exc.Message);
            }
        }
    }
    class Program
    {
        public static void Main(string[] args)
        {
            var logCfg = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config");
            XmlConfigurator.ConfigureAndWatch(logCfg);

            HostFactory.Run(x =>
            {
                x.Service<TownCrier>();
                x.RunAsLocalSystem();

                x.SetDescription(ConfigurationManager.AppSettings["serviceDescription"]);
                x.SetDisplayName(ConfigurationManager.AppSettings["serviceDisplayName"]);
                x.SetServiceName(ConfigurationManager.AppSettings["serviceName"]);
            });
        }
    }

    /*
    public class TownCrier
    {
        readonly Timer _timer;
        public TownCrier()
        {
            _timer = new Timer(1000) { AutoReset = true };
            _timer.Elapsed += (sender, eventArgs) => Console.WriteLine("It is {0} and all is well", DateTime.Now);
        }
        public void Start() { _timer.Start(); }
        public void Stop() { _timer.Stop(); }
    }

    public class Program
    {
        public static void Main()
        {
            HostFactory.Run(x =>                                 //1
            {
                x.Service<TownCrier>(s =>                        //2
                {
                    s.ConstructUsing(name => new TownCrier());     //3
                    s.WhenStarted(tc => tc.Start());              //4
                    s.WhenStopped(tc => tc.Stop());               //5
                });
                x.RunAsLocalSystem();                            //6
      

                                    //启动类型设置
                                    x.StartAutomatically();//自动
                                    x.StartAutomaticallyDelayed();// 自动（延迟启动）
                                    x.StartManually();//手动
                                    x.Disabled();//禁用
                     

                                    x.StartAutomaticallyDelayed();// 自动（延迟启动）

                x.SetDescription("Sample Topshelf Host");        //7
                x.SetDisplayName("Stuff");                       //8
                x.SetServiceName("Stuff");                       //9
            });                                                  //10
        }
    }
     */
}
