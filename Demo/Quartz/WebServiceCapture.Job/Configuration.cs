﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Infrastructure.Lib.Core;
using System.IO;
using System.Xml.Serialization;

namespace WebServiceCapture.Job
{
    public class ConfigUtility
    {
        private static readonly Configuration configuration;

        private static ConcurrentBag<WebserviceClient> _webserviceList;

        public static string SaveBasePath
        {
            get
            {
                return configuration.SaveBasePath;
            }
        }

        public static string MonthPath
        {
            get
            {
                return System.DateTime.Now.ToString("yyMM");
            }
        }

        public static string DatePath
        {
            get
            {
                return System.DateTime.Now.ToString("yyyy-MM-dd");
            }
        }

        public static ConcurrentBag<WebserviceClient> WebserviceList
        {
            get
            {
                if (null == _webserviceList)
                {
                    _webserviceList = new ConcurrentBag<WebserviceClient>();
                    configuration.WebserviceList.ForEach(w =>
                    {
                        _webserviceList.Add(new WebserviceClient
                        {
                            Model = w,
                            Agent = new WebServiceAgent(w.Url)
                        });
                    });
                }
                return _webserviceList;
            }
        }

        static ConfigUtility()
        {
            var xmlPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "wsConfig.xml");
            configuration = SerializeHelper.FromXmlFile<Configuration>(xmlPath);

            //var config = new Configuration
            //{
            //    SaveBasePath = "123",
            //    WebserviceList = new List<WebserviceModel> {
            //       new WebserviceModel{
            //            Id ="1",
            //         Method ="m",
            //         Url = "u",
            //          Args = new List<Arg>{
            //             new Arg{ Value="arg"  }  
            //          }
            //       }
            //    }
            //};

            //var xml = SerializeHelper.ToXml(config);
        }
    }

    [XmlRoot("configuration")]
    public class Configuration
    {
        [XmlElement("saveBasePath")]
        public string SaveBasePath { get; set; }

        [XmlArray("webserviceList"), XmlArrayItem("webservice")]
        public List<WebserviceModel> WebserviceList { get; set; }
        //public ConcurrentBag< MyProperty { get; set; }
    }

    [XmlRoot("webservice")]
    public class WebserviceModel
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("url")]
        public string Url { get; set; }

        [XmlAttribute("method")]
        public string Method { get; set; }

        [XmlArray("args"), XmlArrayItem("arg")]
        public List<Arg> Args { get; set; }
    }

    [XmlRoot("Arg")]
    public class Arg
    {
        [XmlAttribute("value")]
        public string Value { get; set; }
    }
}
