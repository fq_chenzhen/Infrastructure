﻿using Common.Logging;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceCapture.Job
{
    public class WebServiceJobPlugin : ISchedulerPlugin
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(WebServiceJobPlugin));

        public void Initialize(string pluginName, Quartz.IScheduler sched)
        {
            logger.Warn("实例化");
        }

        public void Shutdown()
        {
            logger.Warn("关闭");
        }

        public void Start()
        {
            logger.Warn("启动");
        }
    }
}
