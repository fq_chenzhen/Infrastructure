﻿using Common.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Lib.Core;
using System.IO;

namespace WebServiceCapture.Job
{
    public class WebServiceJob : IJob
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(WebServiceJob));
        public void Execute(IJobExecutionContext context)
        {
            Parallel.ForEach(ConfigUtility.WebserviceList, client =>
            {
                string savePath = Path.Combine(ConfigUtility.SaveBasePath, client.Model.Id, ConfigUtility.MonthPath, ConfigUtility.DatePath);

                FloodWebsiteExecuter.Instance()
                    .SetParameter(client)
                    .SetParameter(savePath)
                    .Initialize()
                    .SaveNotDownloadJpg();

                //new FloodWebSite(savePath, client).SaveNotDownloadJpg();

                //获取路径

                //获取已存在图片列表

                //比对获取列表数据，得到需要下载图片的 url 列表，下载保存到指定目录

            });
        }
    }
};