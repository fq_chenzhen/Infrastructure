﻿using System.IO;
using Topshelf;

namespace Quartz.Server
{
    /// <summary>
    /// The server's main entry point.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main.
        /// </summary>
        public static void Main()
        {
            // change from service account's dir to more logical one
            Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            HostFactory.Run(x =>
                                {
                                    x.RunAsLocalSystem();

                                    x.SetDescription(Configuration.ServiceDescription);
                                    x.SetDisplayName(Configuration.ServiceDisplayName);
                                    x.SetServiceName(Configuration.ServiceName);
                                    /*
                                    //启动类型设置
                                    x.StartAutomatically();//自动
                                    x.StartAutomaticallyDelayed();// 自动（延迟启动）
                                    x.StartManually();//手动
                                    x.Disabled();//禁用
                                    */

                                    x.StartAutomaticallyDelayed();// 自动（延迟启动）

                                    x.Service(factory =>
                                                  {
                                                      QuartzServer server = new QuartzServer();
                                                      server.Initialize();
                                                      return server;
                                                  });
                                });
        }
    }
}