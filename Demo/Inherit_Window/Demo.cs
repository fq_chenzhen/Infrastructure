﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inherit_Window
{
    public partial class Demo : BaseForm
    {
        private LoadingCtl picloadingCtl = new LoadingCtl(162, true);

        public Demo()
        {
            InitializeComponent();
        }

        private void nf1_MouseClick(object sender, MouseEventArgs e)
        {
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        private void Demo_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)  //判断是否最小化
            {
                //nf1.Visible = true;
                nf1.ShowBalloonTip(5000, "系统提示", "托盘测试,正在后台运行中.", ToolTipIcon.Info);
                this.Visible = false;
            }
            else
            {
                //nf1.Visible = false;
                this.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            picloadingCtl.ShowLoading(this.panel1);
            Task.Run(() =>
            {
                System.Threading.Thread.Sleep(3000);
                this.BeginInvoke(new Action(() =>
                {
                    picloadingCtl.Hide();
                }));
            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmFullScreen frmFull = new FrmFullScreen();
            frmFull.ShowDialog();
        }

    }
}
