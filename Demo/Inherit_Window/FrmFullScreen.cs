﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inherit_Window
{
    public partial class FrmFullScreen : Form
    {
        private Size tmpFormSize;
        private FormBorderStyle tmpFormBorderStyle;
        private FormWindowState tmpWindowState;
        private DockStyle tmpDock;

        public FrmFullScreen()
        {
            InitializeComponent();

            Rectangle rect = new Rectangle();
            rect = Screen.GetWorkingArea(this);
            tmpFormSize = new Size(rect.Width, rect.Height);
            tmpFormBorderStyle = FormBorderStyle.None;
            tmpWindowState = FormWindowState.Maximized;
            tmpDock = DockStyle.Fill;
        }

        private void switchFullScreen()
        {
            var vtmpFormSize = this.Size;
            var vtmpFormBorderStyle = this.FormBorderStyle;
            var vtmpWindowState = this.WindowState;
            var vtmpDock = RealPlayWnd.Dock;

            this.Size = tmpFormSize;
            this.FormBorderStyle = tmpFormBorderStyle;
            this.WindowState = tmpWindowState;
            RealPlayWnd.Dock = tmpDock;

            tmpFormSize = vtmpFormSize;
            tmpFormBorderStyle = vtmpFormBorderStyle;
            tmpWindowState = vtmpWindowState;
            tmpDock = vtmpDock;
        }

        private void fullScreen_Load(object sender, EventArgs e)
        {
            switchFullScreen();
        }

        private void fullScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                switchFullScreen();
                this.Close();
            }
        }
    }
}
