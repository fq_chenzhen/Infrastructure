﻿using Dapper.Contrib.Extensions;
using Infrastructure.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProvider
{
    [Serializable]
    public partial class PictureAuthorizeModel
    {
        [Computed]
        public int UsedHours { get; set; }

        [Computed]
        public bool IsAuth
        {
            get
            {
                return this.UsedHours <= this.timeSpan * 24;
            }
        }
    }
}
