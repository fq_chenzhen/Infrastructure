﻿using System;
using Dapper.Contrib.Extensions;

namespace DataProvider
{
    [Table("PicturePlatform")]
	public partial class PicturePlatformModel
    {
        
        [Key]
        public int id { get; set; }

        
        public string name { get; set; }

        
        public string value { get; set; }

    }
}
