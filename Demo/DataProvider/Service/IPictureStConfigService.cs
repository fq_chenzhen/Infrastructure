﻿using Infrastructure.Data.Layer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial interface IPictureStConfigService
    {

        Task<int> Save(IEnumerable<PictureStConfigModel> models);
    }
}
