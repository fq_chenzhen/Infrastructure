﻿using Infrastructure.Data.Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial class PictureDiagnosisService : IPictureDiagnosisService
    {
        public Task<IEnumerable<PictureDiagnosisModel>> QueryTimeRangeAsync(string stcd, string timeRange)
        {
            return (modelDal as IPictureDiagnosisRepository).QueryTimeRangeAsync(stcd, timeRange);
        }


        public Task<IEnumerable<PictureDiagnosisModel>> QueryTimeRangeAsync(string stcd, Infrastructure.Lib.Core.Range<DateTime> timeRange)
        {
            return (modelDal as IPictureDiagnosisRepository).QueryTimeRangeAsync(stcd, timeRange);
        }
    }
}
