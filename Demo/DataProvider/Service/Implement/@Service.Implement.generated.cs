﻿using Infrastructure.Data.Layer;

namespace DataProvider
{
	public partial class PictureAuthorizeService : BaseService<PictureAuthorizeModel>, IPictureAuthorizeService 
    {
        public PictureAuthorizeService(IPictureAuthorizeRepository reps)
        {
            modelDal = reps;
        }
    }

	public partial class PictureDiagnosisService : BaseService<PictureDiagnosisModel>, IPictureDiagnosisService 
    {
        public PictureDiagnosisService(IPictureDiagnosisRepository reps)
        {
            modelDal = reps;
        }
    }

	public partial class PicturePlatformService : BaseService<PicturePlatformModel>, IPicturePlatformService 
    {
        public PicturePlatformService(IPicturePlatformRepository reps)
        {
            modelDal = reps;
        }
    }

	public partial class PictureStConfigService : BaseService<PictureStConfigModel>, IPictureStConfigService 
    {
        public PictureStConfigService(IPictureStConfigRepository reps)
        {
            modelDal = reps;
        }
    }

}
