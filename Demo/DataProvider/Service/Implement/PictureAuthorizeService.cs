﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Lib.Core;

namespace DataProvider
{
    public partial class PictureAuthorizeService : IPictureAuthorizeService
    {

        public string Password
        {
            get
            {
                return "ch!@#456";
            }
        }

        public string Encrypt(string authCode)
        {
            return DesHelper.Encrypt(authCode, Password);
        }

        public string Decrypt(string authCode)
        {
            return DesHelper.Decrypt(authCode, Password);
        }

        public async Task<IEnumerable<PictureAuthorizeModel>> Search(string value)
        {
            string strSql = @"SELECT TOP 1000 *  FROM [PictureAuthorize] where cpuId like @cpuId
                                    union
                                    SELECT TOP 1000 a.*  FROM [PictureAuthorize] a 
                                    inner join [PicturePlatform] b on a.[platform] = b.[value] where b.name like @name";

            var smodel = await modelDal.QueryAsync(strSql, new { cpuId = '%' + value + "%", name = '%' + value + '%' });
            return smodel;
        }
    }
}
