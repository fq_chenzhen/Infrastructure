﻿using Infrastructure.Data.Layer;
using System.Threading.Tasks;
using System.Collections.Generic;
using Infrastructure.Lib.Core;
using System.Linq;

namespace DataProvider
{
    public partial class PictureStConfigService : IPictureStConfigService
    {
        IPictureStConfigRepository _resp;

        public async Task<int> Save(IEnumerable<PictureStConfigModel> models)
        {
            _resp = modelDal as IPictureStConfigRepository;
            return await _resp.Save(models);
        }
    }
}
