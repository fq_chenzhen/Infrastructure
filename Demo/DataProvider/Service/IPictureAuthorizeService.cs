﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial interface IPictureAuthorizeService
    {

        string Password
        {
            get;
        }

        string Encrypt(string source);

        string Decrypt(string source);

        Task<IEnumerable<PictureAuthorizeModel>> Search(string value);
    }
}
