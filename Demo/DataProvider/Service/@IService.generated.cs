﻿using Infrastructure.Data.Layer;

namespace DataProvider
{
	public partial interface IPictureAuthorizeService : IBaseService<PictureAuthorizeModel> { }

	public partial interface IPictureDiagnosisService : IBaseService<PictureDiagnosisModel> { }

	public partial interface IPicturePlatformService : IBaseService<PicturePlatformModel> { }

	public partial interface IPictureStConfigService : IBaseService<PictureStConfigModel> { }

}
