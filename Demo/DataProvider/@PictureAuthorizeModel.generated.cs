﻿using System;
using Dapper.Contrib.Extensions;

namespace DataProvider
{
    [Table("PictureAuthorize")]
	public partial class PictureAuthorizeModel
    {
        
        [Key]
        public int id { get; set; }

        /// <summary>
        /// Cpu编号
        /// </summary>        
        public string cpuId { get; set; }

        
        public string platform { get; set; }

        /// <summary>
        /// 时间间隔（天）
        /// </summary>        
        public int? timeSpan { get; set; }

        /// <summary>
        /// 添加日期
        /// </summary>        
        public DateTime? creDate { get; set; }

    }
}
