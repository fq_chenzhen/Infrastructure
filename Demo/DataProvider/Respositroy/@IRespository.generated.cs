﻿using Infrastructure.Data.Layer;

namespace DataProvider
{
	public partial interface IPictureAuthorizeRepository : IBaseRespository<PictureAuthorizeModel> { }

	public partial interface IPictureDiagnosisRepository : IBaseRespository<PictureDiagnosisModel> { }

	public partial interface IPicturePlatformRepository : IBaseRespository<PicturePlatformModel> { }

	public partial interface IPictureStConfigRepository : IBaseRespository<PictureStConfigModel> { }

}
