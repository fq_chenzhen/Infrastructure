﻿using Infrastructure.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial interface IPictureDiagnosisRepository
    {
        Task<IEnumerable<PictureDiagnosisModel>> QueryTimeRangeAsync(string stcd, string timeRange);

        Task<IEnumerable<PictureDiagnosisModel>> QueryTimeRangeAsync(string stcd, Range<DateTime> timeRange);
    }
}
