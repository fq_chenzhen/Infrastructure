﻿using Dapper;
using Dapper.Contrib.Extensions;
using Infrastructure.Data.Core;
using Infrastructure.Data.Layer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial class PictureStConfigRepository : IPictureStConfigRepository
    {
        public async Task<int> Save(IEnumerable<PictureStConfigModel> models)
        {
            using (var conn = ConnFactory.GetConnection())
            {
                await conn.OpenAsync();
                var trans = conn.BeginTransaction();
                var delResult = await conn.DeleteAllAsync<PictureStConfigModel>(trans);
                //if (delResult)
                //{
                    int i = 0;
                    foreach (var m in models)
                    {
                        await conn.InsertAsync<PictureStConfigModel>(m, trans);
                        i++;
                    }
                    if (i > 0)
                        trans.Commit();
                    else
                        trans.Rollback();
                //}
                //else
                //{
                //    trans.Rollback();
                //}
                return 1;
            }
        }
    }
}
