﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial class PictureDiagnosisRepository : IPictureDiagnosisRepository
    {
        public Task<IEnumerable<PictureDiagnosisModel>> QueryTimeRangeAsync(string stcd, string timeRange)
        {
            var range = new Infrastructure.Lib.Core.Range<DateTime>(timeRange);

            return QueryTimeRangeAsync(stcd, range);
        }


        public Task<IEnumerable<PictureDiagnosisModel>> QueryTimeRangeAsync(string stcd, Infrastructure.Lib.Core.Range<DateTime> timeRange)
        {
            //融桥同一时间发两张图片回来，时间间隔10秒。过滤其中的一张图片，调整为每半个小时一张图片。
            List<string> minuteStcds = new List<string>
            {
                "35016001",
                "35016001-1"
            };

            string strSQL = @"select * from PictureDiagnosis where stcd=@stcd and picDate >=@beginTime and picDate <=@endTime order by picDate asc";

            if (minuteStcds.Contains(stcd))
            {
                strSQL = @"select * from(
                            select row_number() over(partition by convert(datetime,convert(varchar(100),picdate, 23) + ' ' + datename(hh,picdate) + ':' + left(right('0' +  datename(mi, picdate), 2),1) + '0')
                            order by picdate asc) as row, * 
                            from [picturediagnosis] 
                            where stcd=@stcd and picDate >=@beginTime and picDate <=@endTime) a 
                            where row=1 order by picdate asc";
            }

            dynamic p = new System.Dynamic.ExpandoObject();
            p.stcd = stcd;
            p.beginTime = timeRange.LowerBound;
            p.endTime = timeRange.UpperBound;

            //var p = new
            //{
            //    stcd = stcd,
            //    beginTime = range.LowerBound,
            //    endTime = range.UpperBound
            //};
            return QueryAsync(strSQL, p);
        }
    }
}
