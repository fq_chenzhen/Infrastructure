﻿using Infrastructure.Data.Layer;

namespace DataProvider
{
	public partial class PictureAuthorizeRepository : BaseRespository<PictureAuthorizeModel>, IPictureAuthorizeRepository { }

	public partial class PictureDiagnosisRepository : BaseRespository<PictureDiagnosisModel>, IPictureDiagnosisRepository { }

	public partial class PicturePlatformRepository : BaseRespository<PicturePlatformModel>, IPicturePlatformRepository { }

	public partial class PictureStConfigRepository : BaseRespository<PictureStConfigModel>, IPictureStConfigRepository { }

}
