﻿using Infrastructure.Data.Layer;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataProvider
{
    public partial interface IPictureStConfigRepository
    {
        Task<int> Save(IEnumerable<PictureStConfigModel> models);
    }
}
