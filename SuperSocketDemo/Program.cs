﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperSocketDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            GprsTcpHost.Instance.Start();
            GprsUdpHost.Instance.Start();

            Console.ReadLine();

            GprsTcpHost.Instance.Stop();
            GprsUdpHost.Instance.Stop();
        }
    }
}
