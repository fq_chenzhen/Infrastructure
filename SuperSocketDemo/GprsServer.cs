﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace SuperSocketDemo
{
    public class GprsServer : AppServer<GprsSession, BinaryRequestInfo>
    {
        public GprsServer()
            : base(new DefaultReceiveFilterFactory<GprsFilter, BinaryRequestInfo>()) //使用默认的接受过滤器工厂 (DefaultReceiveFilterFactory)
        {
        }

        // todo:直接覆盖事件
        ///// <summary>  
        ///// 输出新连接信息  
        ///// </summary>  
        ///// <param name="session"></param>  
        //protected override void OnNewSessionConnected(GprsSession session)
        //{
        //    base.OnNewSessionConnected(session);
        //    //输出客户端IP地址  
        //    Console.Write("\r\n" + session.LocalEndPoint.Address.ToString() + ":连接");
        //}

        ///// <summary>  
        ///// 输出断开连接信息  
        ///// </summary>  
        ///// <param name="session"></param>  
        ///// <param name="reason"></param>  
        //protected override void OnSessionClosed(GprsSession session, CloseReason reason)
        //{
        //    base.OnSessionClosed(session, reason);
        //    Console.Write("\r\n" + session.LocalEndPoint.Address.ToString() + ":断开连接");
        //}

        //protected override void OnStopped()
        //{
        //    base.OnStopped();
        //    Console.WriteLine("服务已停止");
        //}

        public void appServer_NewSessionConnected(GprsSession session)
        {
            //session.Send("Welcome to SuperSocket Telnet Server!");
        }

        public void appServer_NewRequestReceived(GprsSession session, BinaryRequestInfo requestInfo)
        {
            //Checkout.Check check = new Checkout.Check();
            //var msg = check.byteToHexStr(requestInfo.Body, 0, requestInfo.Body.Length);
            //Socket socket = new Socket(session);

            //AnalysisGPRS analysisGPRS = new AnalysisGPRS();
            //analysisGPRS.Analysis(requestInfo.Body, session);
        }
    }
}
