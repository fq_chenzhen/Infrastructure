﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using SuperSocket.SocketBase.Protocol;

namespace SuperSocketDemo
{
    public class GprsUdpHost : GprsHost
    {
        public static GprsUdpHost Instance = new Lazy<GprsUdpHost>(() => new GprsUdpHost()).Value;

        private GprsUdpHost()
        {
            appServer = new GprsServer();
            appServer.NewSessionConnected += new SessionHandler<GprsSession>(appServer.appServer_NewSessionConnected);
            appServer.NewRequestReceived += new RequestHandler<GprsSession, BinaryRequestInfo>(appServer.appServer_NewRequestReceived);
            
            //启动应用服务端口
            if (!appServer.Setup(new ServerConfig
            {
                Ip = LocalIp,
                LogCommand = true,
                MaxConnectionNumber = 1000,
                Mode = SocketMode.Udp,
                Name = "Udp Gprs Socket Server",
                Port = DataPort,
                MaxRequestLength = 2048
                //ClearIdleSession = true,
                //ClearIdleSessionInterval = 1,
                //IdleSessionTimeOut = 5,
                //SendingQueueSize = 100,
                //ReceiveBufferSize = 50000
            }))
            {
                writeExcptLog.SavaException("服务端口启动失败!");
            }
        }

       
    }
}
