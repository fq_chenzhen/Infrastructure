﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperSocketDemo
{
    public abstract class GprsHost
    {

        protected GprsServer appServer;


        protected string LocalIp
        {
            get
            {
                return  "Any";
            }
        }
        protected int DataPort
        {
            get
            {
                return 5003;
            }
        }

        public void Start()
        {
            if (!appServer.Start())
            {
                Console.WriteLine("服务启动失败!");
                return;
            }
        }

        public void Stop()
        {
            //停止服务
            appServer.Stop();
        }
    }
}
