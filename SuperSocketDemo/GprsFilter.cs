﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperSocket.SocketBase.Protocol;

namespace SuperSocketDemo
{
    public class GprsFilter : IReceiveFilter<BinaryRequestInfo>
    {
        public int LeftBufferSize
        {
            get { return 0; }
        }

        public SuperSocket.SocketBase.Protocol.IReceiveFilter<BinaryRequestInfo> NextReceiveFilter
        {
            get { return this; }
        }

        public SuperSocket.SocketBase.Protocol.FilterState State
        {
            get; private set;
        }

        public BinaryRequestInfo Filter(byte[] readBuffer, int offset, int length, bool toBeCopied, out int rest)
        {
            rest = 0;
            byte[] body = readBuffer.Skip(offset).Take(length).ToArray();
            string strReceiveData = Encoding.UTF8.GetString(body, 0, length);
            return new BinaryRequestInfo(Guid.NewGuid().ToString(), body);
        }

        public void Reset()
        {
        }
    }
}
