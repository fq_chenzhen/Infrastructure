﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SuperSocketDemo
{
    public class Socket
    {
        private GprsSession _gprsSession;
        public Socket(GprsSession gprsSession)
        {
            _gprsSession = gprsSession;
        }

        public int SendTo(byte[] buffer, SocketFlags socketFlags, EndPoint remoteEP)
        {
            _gprsSession.Send(buffer, 0, buffer.Length);
            return buffer.Length;
        }
    }
}
