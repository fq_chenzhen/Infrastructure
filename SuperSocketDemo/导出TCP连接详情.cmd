@rem edit by wc3@istrong.cn
@echo off
@cls
@title 四创科技导出TCP连接详情工具V1.0.0 
@if exist LinkList.txt del LinkList.txt
@set /p port=请输入要查询的端口:
@echo ----------%date% %time%---------- >>LinkList.txt
@echo TCP:%port%端口总连接量 >>LinkList.txt
@netstat -an|find "%port%"|find "ESTABLISHED" /c >>LinkList.txt
@echo TCP连接详情: >>LinkList.txt
@netstat -ano|findstr "TCP"|findstr %port% >> LinkList.txt
@notepad LinkList.txt
